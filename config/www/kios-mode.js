const version = '1.1.1';

class KioskMode extends HTMLElement {
    /*
    constructor() {
        super();
        this.attachShadow({mode: 'open'});
        this.hass = 'test';
    }
    */
    set hass(hass) {
        if (window.kioskMode) {
            return;
        }

        console.log('Starting kiosk mode', version, this, hass);

        window.kioskMode = {
            started: true
        };

        const card = document.querySelector('#ha-kiosk-mode-clock');

        if (!card) {
            this.card = document.createElement('ha-card');
            this.card.setAttribute('id', 'ha-kiosk-mode-clock')
            this.card.header = 'Bezig met laden...';
        } else {
            this.card.innerHTML = '';
        }

        this.content = document.createElement('div');
        this.content.style.padding = '0 1rem 1rem 1rem';
        this.card.appendChild(this.content);
        this.appendChild(this.card);
        // this.shadowRoot.appendChild(this.card);

        if (this.interval) {
            clearInterval(this.interval);
        } else {
            if (!window.kioskMode) {
                this.enableKioskMode();
                this.loadBugSnag();
            }
        }

        this.interval = setInterval(() => {
            this.renderClock();
        }, 60000);
    }

    setConfig(config) {
        if (!config.entity) {
            throw new Error('You need to define an entity');
        }
        this.config = config;
    }

    // The height of your card. Home Assistant uses this to automatically
    // distribute all cards over the available columns.
    getCardSize() {
        return 3;
    }

    renderClock() {
        this.card.header = moment().format('dddd DD/MM/YYYY');
        this.content.innerHTML = '<h1>' + moment().format('HH:mm') + '</h1>';
        this.content.style.textAlign = 'center';
        this.content.style.fontSize = '2rem';
    }

    enableKioskMode() {
        if (window.location.href.indexOf('kiosk') > 0) {

            setTimeout(function () {
                try {
                    const home_assistant_main = document
                        .querySelector("body > home-assistant").shadowRoot
                        .querySelector("home-assistant-main");

                    const header = home_assistant_main.shadowRoot
                        .querySelector("app-drawer-layout > partial-panel-resolver > ha-panel-lovelace").shadowRoot
                        .querySelector("hui-root").shadowRoot
                        .querySelector("#layout > app-header > app-toolbar")
                        .style.display = "none";

                    const drawer = home_assistant_main.shadowRoot
                        .querySelector("#drawer")
                        .style.display = 'none';

                    home_assistant_main.style.setProperty("--app-drawer-width", 0);
                    home_assistant_main.shadowRoot
                        .querySelector("#drawer > ha-sidebar").shadowRoot
                        .querySelector("div.menu > paper-icon-button");
                    // .click();
                    window.dispatchEvent(new Event('resize'));
                } catch (e) {
                    console.log(e);
                }
            }, 200);
        }
    }

    loadBugSnag() {
        const scriptElement = document.createElement('script');
        scriptElement.type = 'text/javascript';
        scriptElement.src = '//d2wy8f7a9ursnm.cloudfront.net/v7/bugsnag.min.js';
        scriptElement.onload = () => {
            Bugsnag.start({ apiKey: '79ad7e8952f5f0eac8e206e7da1618f3' })
        };
        document.head.appendChild(scriptElement);
    }
}

customElements.define('kiosk-mode', KioskMode);
