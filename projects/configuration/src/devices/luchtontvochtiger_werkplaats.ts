import { Device } from '../models'

export const luchtontvochtigerWerkplaats: Device = {
    device_id: '9d75c8c0063811ebab7e2175dc136668',
    alias: 'Luchtontvochtiger Werkplaats',
    entities: {
        switch: {
            entity_id: 'switch.40605414bcddc28b9bb5',
            alias: 'tuya_switch_5',
        },
        humidity_above_50: {
            entity_id: 'binary_sensor.humidity_workshop_above_50',
        },
    },
}
