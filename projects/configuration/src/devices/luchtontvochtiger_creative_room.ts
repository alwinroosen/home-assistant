import { Device } from '../models'

export const luchtontvochtigerCreativeRoom: Device = {
    device_id: '9d724efc063811eba8e7b11343ee739d',
    alias: 'Luchtontvochtiger Creative Room',
    entities: {
        switch: {
            entity_id: 'switch.80020737bcddc28b9b33',
            alias: 'tuya_switch_1',
        },
        humidity_above_50: {
            entity_id: 'binary_sensor.humidity_creative_room_above_50',
        },
    },
}
