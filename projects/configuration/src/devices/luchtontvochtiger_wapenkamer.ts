import { Device } from '../models'

export const luchtontvochtigerWapenkamer: Device = {
    device_id: '9d74a599063811ebaf98f9d98ef36a87',
    alias: 'Luchtontvochtiger Wapenkamer',
    entities: {
        switch: {
            entity_id: 'switch.80020737b4e62d42508b',
            alias: 'tuya_switch_7',
        },
        humidity_above_50: {
            entity_id: 'binary_sensor.humidity_wapenkamer_above_50',
        },
    },
}
