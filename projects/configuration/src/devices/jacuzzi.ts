import { Device } from '../models'

export const jacuzzi: Device = {
    device_id: '9d724efc063811eba8e7b11343ee628c',
    alias: 'Jacuzzi',
    entities: {
        switch: {
            entity_id: 'switch.80020737bcddc28b9b27',
            alias: 'tuya_switch_4',
        },
        jacuzzi_on: {
            entity_id: 'binary_sensor.jacuzzi_on',
        },
        jacuzzi_heater_on: {
            entity_id: 'binary_sensor.jacuzzi_heater_on',
        },
    },
}
