import { Device } from '../models'

export const luchtontvochtigerMagazijn: Device = {
    device_id: '9d739794063811eb998cf37e66112501',
    alias: 'Luchtontvochtiger Magazijn',
    entities: {
        switch: {
            entity_id: 'switch.37685021bcddc28b9c68',
            alias: 'tuya_switch_3',
        },
        humidity_above_50: {
            entity_id: 'binary_sensor.humidity_magazijn_above_50',
        },
    },
}
