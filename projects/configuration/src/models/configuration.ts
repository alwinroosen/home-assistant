import {
    BinarySensors,
    LoveLace,
    HomeConnect,
    SensorCollection,
    CameraCollection,
    Influxdb,
    Nest,
    GroupCollection,
    AutomationCollection,
    Bring,
    RestCommandCollection,
} from './index'
import { UtilityMeter } from './utilityMeter'

export interface Configuration {
    default_config: null
    tts: {
        platform: 'google_translate'
    }[]
    frontend?: {
        themes: string
    }
    lovelace: LoveLace
    home_connect: HomeConnect
    sensor: SensorCollection
    camera: CameraCollection
    influxdb: Influxdb
    nest: Nest
    binary_sensor: BinarySensors[]
    group: GroupCollection
    automation: AutomationCollection
    bring_shopping_list: Bring
    rest_command: RestCommandCollection
    utility_meter: UtilityMeter
}
