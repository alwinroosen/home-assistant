interface SwitchAction {
    type: 'turn_off' | 'turn_on'
    device_id: string
    entity_id: string
    domain: 'switch'
}

interface NotifyAction {
    service: string
    data: {
        message: string
    }
}

type Action = SwitchAction | NotifyAction

interface TimeTrigger {
    platform: 'time'
    at: string
}

interface StateTrigger {
    platform: 'state'
    entity_id: string
    to: 'on' | 'off'
}

type Trigger = TimeTrigger | StateTrigger

interface Condition {
    condition: 'state'
    entity_id: string
    state: string
}

export interface Automation {
    id: string
    alias: string
    description: string
    trigger: Trigger
    condition: Condition[]
    action: Action[]
    mode: 'single'
}

export type AutomationCollection = Automation[]
