interface SwitchAction {
    type: 'turn_off' | 'turn_on'
    device_id: string
    entity_id: string
    domain: 'switch'
}

interface NotifyAction {
    service: string
    data: {
        message: string
    }
}

interface LightToggleAction {
    service: 'light.toggle'
    target: {
        entity_id: string
    }
}

interface LightTurnOnAction {
    service: 'light.turn_on'
    target: {
        entity_id: string
    }
}

interface LightTurnOffAction {
    service: 'light.turn_off'
    target: {
        entity_id: string
    }
}

interface SwitchTurnOnAction {
    service: 'switch.turn_on'
    target: {
        entity_id: string
    }
}

interface ServiceTargetAction {
    service: string
    target: {
        entity_id: string
    }
}

interface DomainAction {
    domain: 'light'
    type: 'turn_on' | 'turn_off'
    device_id: string
    entity_id: string
}

export interface ServiceAction {
    service: string
    data: any
}

export type Action =
    | ServiceTargetAction
    | SwitchAction
    | NotifyAction
    | LightToggleAction
    | SwitchTurnOnAction
    | LightTurnOnAction
    | LightTurnOffAction
    | DomainAction
    | ServiceAction

interface TimeTrigger {
    platform: 'time'
    at: string
}

interface StateTrigger {
    platform: 'state'
    entity_id: string
    to: 'on' | 'off'
}

interface NumericStateTriggerCommon {
    platform: 'numeric_state'
    entity_id: string
    for: {
        hours: number
        minutes: number
        seconds: number
    }
    attribute?: 'unit_of_measurement'
}

interface NumericStateTriggerAbove extends NumericStateTriggerCommon {
    above: number
}

interface NumericStateTriggerBelow extends NumericStateTriggerCommon {
    below: number
}

export type NumericStateTrigger =
    | NumericStateTriggerAbove
    | NumericStateTriggerBelow

export interface EventTrigger {
    platform: 'event'
    event_type: string
    event_data: any
}

interface DeviceTrigger {
    platform: 'device'
    type: 'motion' | 'powered' | 'not_powered'
    device_id: string
    entity_id: string
    domain: 'binary_sensor'
}

interface HueDeviceTrigger {
    device_id: string
    domain: 'hue'
    platform: 'device'
    type: 'remote_button_short_release'
    subtype: 'turn_on' | 'turn_off'
}

type Trigger =
    | TimeTrigger
    | StateTrigger
    | EventTrigger
    | DeviceTrigger
    | HueDeviceTrigger
    | NumericStateTrigger

export interface DeviceCondition {
    condition: 'device'
    device_id: string
    entity_id: string
    domain: 'switch'
    type: 'is_off' | 'is_on'
}

export interface StateCondition {
    condition: 'state'
    entity_id: string
    state: string
}

type Condition = StateCondition | DeviceCondition

export interface Automation {
    id: string
    alias: string
    description: string
    trigger: Trigger
    condition: Condition[]
    action: Action[]
    mode: 'single' | 'queued'
    max?: number
}

export type AutomationCollection = Automation[]
