interface Group {
    name: string
    entities: string[]
}

export type GroupCollection = Record<string, Group>
