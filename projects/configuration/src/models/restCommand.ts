export interface RestCommand {
    url: string
    method: 'POST' | 'PUT'
    content_type: string
    payload: string
    headers: Record<string, string>
    verify_ssl: false
}

export type RestCommandCollection = Record<string, RestCommand>
