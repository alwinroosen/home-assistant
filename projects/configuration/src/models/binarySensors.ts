export interface BinarySensor {
    friendly_name: string
    value_template: string
    delay_off?: {
        minutes: number
    }
}

export type BinarySensorCollection = Record<string, BinarySensor>

export interface BinarySensors {
    platform: 'template'
    sensors: BinarySensorCollection
}
