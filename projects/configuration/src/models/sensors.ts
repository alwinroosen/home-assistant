import { UnitOfMeasure } from './index'

interface Sensor {
    platform: 'command_line' | 'pvoutput' | 'template' | 'dsmr'
    name?: string
}

export interface CommandLineSensor extends Sensor {
    platform: 'command_line'
    command?: string
    unit_of_measurement?: string
    value_template?: string
}

export interface PvoutputSensor extends Omit<Sensor, 'name'> {
    platform: 'pvoutput'
    system_id: number
    api_key: string
    scan_interval: number
}

interface TemplateSensorEntry {
    friendly_name: string
    value_template: string
    unit_of_measurement?: UnitOfMeasure
}

export interface TemplateSensor extends Sensor {
    platform: 'template'
    sensors: Record<string, TemplateSensorEntry>
}

export interface DsmrSensor extends Sensor {
    platform: 'dsmr'
    port: string
    dsmr_version: string
}

export interface RestSensor {
    platform: 'rest'
    authentication: 'basic' | 'digest'
    username: string
    password: string
    scan_interval: number
    resource: string
    headers: Record<string, string>
    name: string
    value_template: string
    json_attributes_path: string
    json_attributes: string[]
}

export type SensorCollection = (
    | CommandLineSensor
    | PvoutputSensor
    | TemplateSensor
    | DsmrSensor
    | RestSensor
)[]
