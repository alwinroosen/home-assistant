export interface Nest {
    client_id: string
    client_secret: string
    project_id: string
    subscriber_id: string
}
