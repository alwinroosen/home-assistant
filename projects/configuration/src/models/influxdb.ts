export interface Influxdb {
    api_version: number
    ssl: boolean
    host: string
    port: number
    token: string
    organization: string
    bucket: string
}
