export interface BringList {
    id: string
    name: string
    locale: 'en-US' | 'nl-BE' | 'nl-NL'
}

export interface Bring {
    lists: BringList[]
}
