export interface Camera {
    platform: 'generic'
    still_image_url: string
    stream_source: string
    name: string
    framerate: number
}

export type CameraCollection = Camera[]
