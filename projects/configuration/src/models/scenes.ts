export interface Scene {
    name: string
    entities: Record<string, any>
}
