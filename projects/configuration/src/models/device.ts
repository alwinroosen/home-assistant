type EntityName = string

interface Entity {
    entity_id: string
    alias?: string
}

export type Device = {
    device_id: string
    alias: string
    entities: Record<EntityName, Entity>
}
