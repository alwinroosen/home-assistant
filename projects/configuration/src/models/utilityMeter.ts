export interface UtilityMeterEntry {
    source: string
    cycle:
        | 'quarter-hourly'
        | 'hourly'
        | 'daily'
        | 'weekly'
        | 'monthly'
        | 'yearly'
}

export type UtilityMeter = Record<string, UtilityMeterEntry>
