export interface LoveLaceResource {
    type: 'module' | 'js'
    url: string
}

type DashboardName = string

interface Dashboard {
    mode: 'yaml'
    filename: string
    title: string
    icon?: string // https://materialdesignicons.com/
    show_in_sidebar?: boolean
    require_admin?: boolean
}

type Dashboards = Record<DashboardName, Dashboard>

export interface LoveLace {
    mode: 'yaml'
    resources: LoveLaceResource[]
    dashboards: Dashboards
}
