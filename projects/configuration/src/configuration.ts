import { config } from 'dotenv'

config()

import * as YAML from 'yaml'
import * as fs from 'fs'
import * as path from 'path'

import {
    binary_sensor,
    camera,
    home_connect,
    sensor,
    lovelace,
    influxdb,
    nest,
    group,
    automation,
    // bring_shopping_list,
    rest_command,
    utility_meter,
} from './data'
import { Configuration } from './models'
import { scene } from './data'

const configuration: Configuration = {
    default_config: null,
    tts: [
        {
            platform: 'google_translate',
        },
    ],
    lovelace,
    // home_connect,
    sensor,
    camera,
    // influxdb,
    // nest,
    binary_sensor,
    group,
    automation,
    // bring_shopping_list,
    rest_command,
    utility_meter,
    template: [
        {
            sensor: [
                {
                    name: 'energy_production_sma_total_yield',
                    // friendly_name: 'Totale opbrengst zonnepanelen',
                    unit_of_measurement: 'kWh',
                    state: `{{ states('sensor.sma_total_yield')|float }}`,
                    device_class: 'energy',
                    state_class: 'total_increasing',
                    attributes: {
                        last_reset: '1970-01-01T00:00:00+00:00',
                    },
                },
            ],
        },
    ],
    scene,
}

const buildDir = path.resolve('.', 'dist')

if (!fs.existsSync(buildDir)) {
    fs.mkdirSync(buildDir)
}

fs.writeFileSync(
    path.resolve(buildDir, 'configuration.yaml'),
    YAML.stringify(configuration, {
        schema: 'json',
    }).replace(/"default_config/, '"my":\n"default_config'),
)
