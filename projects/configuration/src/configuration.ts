import { config } from 'dotenv'

config()

import * as YAML from 'yaml'
import * as fs from 'fs'
import * as path from 'path'

import {
    binary_sensor,
    camera,
    home_connect,
    sensor,
    lovelace,
    influxdb,
    nest,
    group,
    automation,
    bring_shopping_list,
    rest_command,
    utility_meter,
} from './data'
import { Configuration } from './models'

const configuration: Configuration = {
    default_config: null,
    tts: [
        {
            platform: 'google_translate',
        },
    ],
    lovelace,
    home_connect,
    sensor,
    camera,
    influxdb,
    nest,
    binary_sensor,
    group,
    automation,
    bring_shopping_list,
    rest_command,
    utility_meter,
}

const buildDir = path.resolve('.', 'dist')

if (!fs.existsSync(buildDir)) {
    fs.mkdirSync(buildDir)
}

fs.writeFileSync(
    path.resolve(buildDir, 'configuration.yaml'),
    YAML.stringify(configuration, {
        schema: 'json',
    }),
)
