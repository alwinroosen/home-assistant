import { Action, Automation, EventTrigger } from '../../models'

enum HueButton {
    ON = 1,
    UP = 2,
    DOWN = 3,
    OFF = 4,
}

const triggerHueSwitchPressed = (button: HueButton): EventTrigger => {
    return {
        platform: 'event',
        event_type: 'hue_event',
        event_data: {
            device_id: '70ca198e432a64c4f87503bb5d4608a0',
            type: 'initial_press',
            subtype: button,
        },
    }
}

const actionsLightState = (state: 'on' | 'off'): Action[] => {
    return [
        {
            service: `light.turn_${state}`,
            target: {
                entity_id: 'light.lenn_1',
            },
        },
        {
            service: `light.turn_${state}`,
            target: {
                entity_id: 'light.lenn_2',
            },
        },
    ]
}

const actionsLightScene = (scene: 'rgb' | 'wit') => {
    return [
        {
            service: 'scene.turn_on',
            target: {
                entity_id: `scene.slaapkamer_lenn_${scene}_licht`,
            },
        },
    ]
}

const lightsLennOn: Automation = {
    id: '7f18be62-9962-485e-b36e-fae2bfed09de',
    alias: 'Lenn licht AAN',
    description: '',
    trigger: triggerHueSwitchPressed(HueButton.ON),
    condition: [],
    action: actionsLightState('on'),
    mode: 'single',
}

const lightsLennOff: Automation = {
    id: '41de403e-4bd7-4b7e-98b8-3381ed4ce0ce',
    alias: 'Lenn licht UIT',
    description: '',
    trigger: triggerHueSwitchPressed(HueButton.OFF),
    condition: [],
    action: actionsLightState('off'),
    mode: 'single',
}

const lightsLennSceneWit: Automation = {
    id: '00975769-9454-4fed-b411-d6169c590f33',
    alias: 'Lenn licht WIT',
    description: '',
    trigger: triggerHueSwitchPressed(HueButton.UP),
    condition: [],
    action: actionsLightScene('wit'),
    mode: 'single',
}

const lightsLennSceneRgb: Automation = {
    id: '35c439e3-1cb0-4907-a859-f9ae23eb5bfa',
    alias: 'Lenn licht RGB',
    description: '',
    trigger: triggerHueSwitchPressed(HueButton.DOWN),
    condition: [],
    action: actionsLightScene('rgb'),
    mode: 'single',
}

export const lightsLennAutomations: Automation[] = [
    lightsLennOn,
    lightsLennOff,
    lightsLennSceneWit,
    lightsLennSceneRgb,
]
