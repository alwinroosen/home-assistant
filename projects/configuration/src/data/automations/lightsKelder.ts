import { Automation } from '../../models'

const notifyService = 'notify.mobile_app_sm_g998b'

const kelderSwitchPressed: Automation = {
    id: '34a322ee-b6ef-49e5-ac5e-98fa16730443',
    alias: 'Kelder Switch Pressed',
    description: '',
    trigger: {
        platform: 'event',
        event_type: 'shelly.click',
        event_data: {
            device: 'shellybutton1-E8DB84ACBBEB',
            channel: 1,
            click_type: 'single',
        },
    },
    condition: [],
    action: [
        {
            service: notifyService,
            data: {
                message: 'Kelder Switch Pressed',
            },
        },
        {
            service: 'light.toggle',
            target: {
                entity_id: 'light.shelly1_98cdac24c80a_2',
            },
        },
        {
            service: 'light.toggle',
            target: {
                entity_id: 'light.shelly1_e8db84aaca5b',
            },
        },
        {
            service: 'light.toggle',
            target: {
                entity_id: 'light.shelly1_c45bbe5f5844',
            },
        },
        {
            service: 'light.toggle',
            target: {
                entity_id: 'light.shelly1_98cdac254513',
            },
        },
    ],
    mode: 'single',
}

const kelderSwitchDoublePressed: Automation = {
    id: '34a322ee-b6ef-49e5-ac5e-98fa16730444',
    alias: 'Kelder Switch Double Pressed',
    description: '',
    trigger: {
        platform: 'event',
        event_type: 'shelly.click',
        event_data: {
            device: 'shellybutton1-E8DB84ACBBEB',
            channel: 1,
            click_type: 'double',
        },
    },
    condition: [],
    action: [
        {
            service: notifyService,
            data: {
                message: 'Kelder Switch Double Pressed',
            },
        },
        {
            service: 'switch.turn_on',
            target: {
                entity_id: 'switch.shelly1_98cdac24a8ed',
            },
        },
    ],
    mode: 'single',
}

const kelderMotionDetected: Automation = {
    id: '4edc416a-98ff-48e7-b5e1-1b0cadccca17',
    alias: 'Kelder Motion Detected',
    description: 'Motion is detected in shellyPlugs, turn on lights',
    trigger: {
        platform: 'device',
        type: 'motion',
        device_id: '1e2b00abeb1dd179e5f94c319684f820',
        entity_id: 'binary_sensor.shellymotionsensor_60a423c675fc_motion',
        domain: 'binary_sensor',
    },
    condition: [
        {
            condition: 'state',
            entity_id: 'binary_sensor.shellyix3_98cdac24c3cd_channel_1_input',
            state: 'off',
        },
    ],
    action: [
        {
            service: notifyService,
            data: {
                message: 'Kelder Motion Detected',
            },
        },
        {
            service: 'light.toggle',
            target: {
                entity_id: 'light.shelly1_98cdac24c80a_2',
            },
        },
        {
            service: 'light.toggle',
            target: {
                entity_id: 'light.shelly1_e8db84aaca5b',
            },
        },
        {
            service: 'light.toggle',
            target: {
                entity_id: 'light.shelly1_c45bbe5f5844',
            },
        },
        {
            service: 'light.toggle',
            target: {
                entity_id: 'light.shelly1_98cdac254513',
            },
        },
    ],
    mode: 'single',
}

const kelderSwitchI31On: Automation = {
    id: 'e4c1e662-3661-4b9e-8662-9ddaa5ab1f3b',
    alias: 'Kelder Switch i3 ON',
    description: '',
    trigger: {
        type: 'powered',
        platform: 'device',
        device_id: '95e69c3209146300eed8eb8578056630',
        entity_id: 'binary_sensor.shellyix3_98cdac24c3cd_channel_1_input',
        domain: 'binary_sensor',
    },
    condition: [],
    action: [
        {
            domain: 'light',
            type: 'turn_on',
            device_id: '7c6a12ebe5d7bafe10a313de44ecd89f',
            entity_id: 'light.shelly1_98cdac24c80a_2',
        },
        {
            type: 'turn_on',
            device_id: 'ec67cf64987016098e20f2c846b33769',
            entity_id: 'light.shelly1_e8db84aaca5b',
            domain: 'light',
        },
        {
            type: 'turn_on',
            device_id: '23ccc1d5697f2144dc30afbf8b552da4',
            entity_id: 'light.shelly1_c45bbe5f5844',
            domain: 'light',
        },
        {
            type: 'turn_on',
            device_id: '39dada6991e3a1a8294457241cdf038c',
            entity_id: 'light.shelly1_98cdac254513',
            domain: 'light',
        },
    ],
    mode: 'single',
}

const kelderSwitchI31Off: Automation = {
    id: '55a991e3-0b4b-4e47-b73c-fe4ab8f757c3',
    alias: 'Kelder Switch i3 OFF',
    description: '',
    trigger: {
        type: 'not_powered',
        platform: 'device',
        device_id: '95e69c3209146300eed8eb8578056630',
        entity_id: 'binary_sensor.shellyix3_98cdac24c3cd_channel_1_input',
        domain: 'binary_sensor',
    },
    condition: [],
    action: [
        {
            domain: 'light',
            type: 'turn_off',
            device_id: '7c6a12ebe5d7bafe10a313de44ecd89f',
            entity_id: 'light.shelly1_98cdac24c80a_2',
        },
        {
            type: 'turn_off',
            device_id: 'ec67cf64987016098e20f2c846b33769',
            entity_id: 'light.shelly1_e8db84aaca5b',
            domain: 'light',
        },
        {
            type: 'turn_off',
            device_id: '23ccc1d5697f2144dc30afbf8b552da4',
            entity_id: 'light.shelly1_c45bbe5f5844',
            domain: 'light',
        },
        {
            type: 'turn_off',
            device_id: '39dada6991e3a1a8294457241cdf038c',
            entity_id: 'light.shelly1_98cdac254513',
            domain: 'light',
        },
    ],
    mode: 'single',
}

export const lightsKelderAutomations: Automation[] = [
    kelderSwitchPressed,
    kelderSwitchDoublePressed,
    kelderMotionDetected,
    kelderSwitchI31On,
    kelderSwitchI31Off,
]
