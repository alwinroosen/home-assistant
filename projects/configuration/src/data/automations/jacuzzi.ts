import {
    Automation,
    DeviceCondition,
    NumericStateTrigger,
    ServiceAction,
    StateCondition,
} from '../../models'
import { jacuzzi } from '../../devices/jacuzzi'

const notifyService = 'notify.mobile_app_sm_g998b'

const triggerOn: NumericStateTrigger = {
    platform: 'numeric_state',
    entity_id: 'sensor.power_generation_vs_consumption',
    for: {
        hours: 0,
        minutes: 1,
        seconds: 0,
    },
    above: 2,
}

const triggerOff: NumericStateTrigger = {
    platform: 'numeric_state',
    entity_id: 'sensor.power_generation_vs_consumption',
    for: {
        hours: 0,
        minutes: 0,
        seconds: 10,
    },
    below: 0.5,
}

const conditionAutomationActive: StateCondition = {
    condition: 'state',
    entity_id: 'input_boolean.jacuzzi_auto',
    state: 'on',
}

const conditionTurnedOff: DeviceCondition = {
    condition: 'device',
    type: 'is_off',
    device_id: '9d713ae2063811eb8b38f10fe6a0ed48',
    entity_id: 'switch.80020737bcddc28b9b27',
    domain: 'switch',
}

const conditionTurnedOn: DeviceCondition = {
    condition: 'device',
    type: 'is_on',
    device_id: '9d713ae2063811eb8b38f10fe6a0ed48',
    entity_id: 'switch.80020737bcddc28b9b27',
    domain: 'switch',
}

const actionTurnOn: ServiceAction = {
    service: 'rest_command.intex_spa_service_turn_on',
    data: {},
}

const actionTurnOff: ServiceAction = {
    service: 'rest_command.intex_spa_service_turn_off',
    data: {},
}

export const automationJacuzziOn: Automation = {
    id: 'jacuzzi_auto_on',
    alias: 'Jacuzzi AAN',
    description:
        'Zet de jacuzzi aan wanneer er meer dan een minuut meer dan 2kW overschot aan stroom is',
    trigger: triggerOn,
    condition: [conditionAutomationActive, conditionTurnedOff],
    action: [actionTurnOn],
    mode: 'queued',
    max: 3,
}

export const automationJacuzziOff: Automation = {
    id: 'jacuzzi_auto_off',
    alias: 'Jacuzzi UIT',
    description:
        'Zet de jacuzzi uit wanneer er meer dan 10 seconden minder dan 2kW overschot aan stroom is',
    trigger: triggerOff,
    condition: [conditionAutomationActive, conditionTurnedOn],
    action: [actionTurnOff],
    mode: 'single',
}

export const jacuzziAutomations: Automation[] = [
    {
        id: '123456789012',
        mode: 'single',
        alias: 'Notification wanneer jacuzzi heater aan gaat',
        description: '',
        condition: [],
        trigger: {
            platform: 'state',
            entity_id: jacuzzi.entities.jacuzzi_heater_on.entity_id,
            to: 'on',
        },
        action: [
            {
                service: notifyService,
                data: {
                    message: 'Jacuzzi verwarming AAN',
                },
            },
        ],
    },
    {
        id: '123456789013',
        mode: 'single',
        alias: 'Notification wanneer jacuzzi heater uit gaat',
        description: '',
        condition: [],
        trigger: {
            platform: 'state',
            entity_id: jacuzzi.entities.jacuzzi_heater_on.entity_id,
            to: 'off',
        },
        action: [
            {
                service: notifyService,
                data: {
                    message: 'Jacuzzi verwarming UIT',
                },
            },
        ],
    },
    {
        id: '123456789014',
        mode: 'single',
        alias: 'Notification wanneer jacuzzi aan gaat',
        description: '',
        condition: [],
        trigger: {
            platform: 'state',
            entity_id: jacuzzi.entities.jacuzzi_on.entity_id,
            to: 'on',
        },
        action: [
            {
                service: notifyService,
                data: {
                    message: 'Jacuzzi AAN',
                },
            },
        ],
    },
    {
        id: '123456789015',
        mode: 'single',
        alias: 'Notification wanneer jacuzzi uit gaat',
        description: '',
        condition: [],
        trigger: {
            platform: 'state',
            entity_id: jacuzzi.entities.jacuzzi_on.entity_id,
            to: 'off',
        },
        action: [
            {
                service: notifyService,
                data: {
                    message: 'Jacuzzi UIT',
                },
            },
        ],
    },
    automationJacuzziOn,
    automationJacuzziOff,
]
