import { BinarySensor, BinarySensors } from '../models'

const greaterThanZero = (
    friendlyName: string,
    sensorName: string,
): BinarySensor => {
    return {
        friendly_name: friendlyName,
        value_template: "{{ states('sensor." + sensorName + "')|float > 0 }}",
    }
}

const greaterOrEqualThan = (
    friendlyName: string,
    sensorName: string,
    threshold: number,
): BinarySensor => {
    return {
        friendly_name: friendlyName,
        delay_off: {
            minutes: 5,
        },
        value_template:
            "{{ states('sensor." +
            sensorName +
            "')|float >= " +
            threshold +
            ' }}',
    }
}

const binarySensors: BinarySensors[] = [
    {
        platform: 'template',
        sensors: {
            generating_more_power_than_consumption: greaterThanZero(
                'Meer opbrengst dan verbuik',
                'power_generation_vs_consumption',
            ),
            humidity_workshop_above_50: greaterOrEqualThan(
                'Vochtigheidsgraad Werkplaats boven 50%',
                'workshop_humidity',
                50,
            ),
            humidity_workshop_above_60: greaterOrEqualThan(
                'Vochtigheidsgraad Werkplaats boven 60%',
                'workshop_humidity',
                60,
            ),
            humidity_creative_room_above_50: greaterOrEqualThan(
                'Vochtigheidsgraad Creative Room boven 50%',
                'creative_room_humidity',
                50,
            ),
            humidity_creative_room_above_60: greaterOrEqualThan(
                'Vochtigheidsgraad Creative Room boven 60%',
                'creative_room_humidity',
                60,
            ),
            humidity_magazijn_above_50: greaterOrEqualThan(
                'Vochtigheidsgraad Magazijn boven 50%',
                'magazijn_humidity',
                50,
            ),
            humidity_magazijn_above_60: greaterOrEqualThan(
                'Vochtigheidsgraad Magazijn boven 60%',
                'magazijn_humidity',
                60,
            ),
            humidity_wapenkamer_above_50: greaterOrEqualThan(
                'Vochtigheidsgraad Wapenkamer boven 50%',
                'wapenkamer_humidity',
                50,
            ),
            humidity_wapenkamer_above_60: greaterOrEqualThan(
                'Vochtigheidsgraad Wapenkamer boven 60%',
                'wapenkamer_humidity',
                60,
            ),
            jacuzzi_on: greaterOrEqualThan(
                'Jacuzzi turned on',
                'tuya_switch_4_power',
                30,
            ),
            jacuzzi_heater_on: greaterOrEqualThan(
                'Jacuzzi heater turned on',
                'tuya_switch_4_power',
                500,
            ),
        },
    },
]

export default binarySensors
