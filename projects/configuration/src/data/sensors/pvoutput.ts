import { PvoutputSensor } from '../../models'

export const pvoutput: PvoutputSensor = {
    platform: 'pvoutput',
    system_id: 77439,
    api_key: process.env['PVOUTPUT_API_KEY'],
    scan_interval: 120,
}
