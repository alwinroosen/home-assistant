import { RestSensor } from '../../models'

export const intexSensor: RestSensor = {
    platform: 'rest',
    authentication: 'basic',
    username: 'admin',
    password: process.env['INTEX_REST_PASSWORD'],
    scan_interval: 15,
    headers: {
        'User-Agent': 'Home Assistant',
        'Content-Type': 'application/json',
    },
    name: `Intex SPA State`,
    resource: 'http://192.168.12.116:8080/state',
    json_attributes_path: '$.state',
    value_template: '{{ value_json.state.status }}',
    json_attributes: [
        'currentTemperature',
        'targetTemperature',
        'status',
        'minTemperature',
        'maxTemperature',
        'outsideTemperature',
        'outsideHumidity',
    ],
}
