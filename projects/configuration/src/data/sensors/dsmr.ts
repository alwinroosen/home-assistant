import { DsmrSensor } from '../../models'

export const dsmr: DsmrSensor = {
    platform: 'dsmr',
    port: '/dev/ttyUSB0',
    dsmr_version: '5B',
}
