import { Bring } from '../models'

const bring: Bring = {
    lists: [
        {
            id: '5e86ced1-570a-4944-a792-03dcdd931fcc',
            name: 'Winkellijstje',
            locale: 'nl-NL',
        },
    ],
}

export default bring
