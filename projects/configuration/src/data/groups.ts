import { GroupCollection } from '../models'

const groups: GroupCollection = {
    kitchen_lights: {
        name: 'Licht Keuken',
        entities: [
            'light.hue_ambiance_spot_1_3',
            'light.hue_ambiance_spot_1_4',
            'light.hue_ambiance_spot_2_3',
            'light.hue_ambiance_spot_2_4',
            'light.hue_ambiance_spot_3_3',
        ],
    },
    living_lights: {
        name: 'Licht Living',
        entities: [
            'light.hue_ambiance_spot_1',
            'light.hue_ambiance_spot_2',
            'light.hue_ambiance_spot_3',
            'light.hue_ambiance_spot_4',
            'light.hue_ambiance_spot_6',
            'light.hue_ambiance_spot_7',
            'light.hue_filament_bulb_1',
            'light.hue_filament_bulb_1_2',
            'light.hue_filament_bulb_2',
        ],
    },
    meter_readings: {
        name: 'Meter readings',
        entities: [
            'sensor.energy_consumption_tarif_1',
            'sensor.energy_consumption_tarif_2',
            'sensor.energy_production_tarif_1',
            'sensor.energy_production_tarif_2',
            'sensor.gas_consumption',
        ],
    },
    twcmanager: {
        name: 'TWC Manager',
        entities: [
            'sensor.twcmanager_all_total_amps_in_use',
            'sensor.twcmanager_all_max_amps_for_slaves',
            'sensor.twcmanager_9177_amps_in_use',
            'sensor.twcmanager_9177_amps_max',
            'sensor.twcmanager_9177_cars_charging',
            'sensor.twcmanager_9177_current_vehicle_vin',
            'sensor.twcmanager_9177_last_vehicle_vin',
            'sensor.twcmanager_9177_lifetime_kwh',
            'sensor.twcmanager_9177_power',
            'sensor.twcmanager_9177_state',
            'sensor.twcmanager_9177_voltage_phase_phase',
            'sensor.twcmanager_config_min_amps_per_twc',
            'sensor.twcmanager_config_max_amps_for_slaves',
        ],
    },
}

export default groups
