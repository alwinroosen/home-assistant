import { TemplateSensor } from '../models'
import { dsmr } from './sensors/dsmr'
import { intexSensor } from './sensors/intex'
import { pvoutput } from './sensors/pvoutput'
import { cpuTemperature } from './sensors/cpuTemperature'

const generateTwcmanagerStateFriendly = () => {
    const knownStates = {
        0: 'Ready',
        1: 'Charging',
        2: 'Error',
        3: 'Plugged in',
        4: 'Plugged in',
        5: 'Busy',
        6: 'TBA',
        7: 'TBA',
        8: 'Start Charge',
        9: 'TBA',
        10: 'Amp Adj.',
    }
    let valueTemplateParts = []
    for (let state in knownStates) {
        if (knownStates.hasOwnProperty(state)) {
            valueTemplateParts.push(
                `states("sensor.twcmanager_9177_state") == "${state}" %}${knownStates[state]}`,
            )
        }
    }
    return `{% if ${valueTemplateParts.join(
        '{% elif ',
    )}{% else %}Unknown{% endif %}`
}

const tarifPowerConsumption = 0.25
const tarifGasConsumption = 0.8
const tarifPowerProduction = 0.0421

const powerGasConsumptionSensors = {
    power_consumption_daily: {
        friendly_name: 'Verbruik vandaag',
        unit_of_measurement: 'kWh',
        value_template:
            "{{ states('sensor.consumption_tarif_1_daily')|float + states('sensor.consumption_tarif_2_daily')|float }}",
    },
    power_consumption_monthly: {
        friendly_name: 'Verbruik deze maand',
        unit_of_measurement: 'kWh',
        value_template:
            "{{ states('sensor.consumption_tarif_1_monthly')|float + states('sensor.consumption_tarif_2_monthly')|float }}",
    },
    power_production_daily: {
        friendly_name: 'Opbrengst vandaag',
        unit_of_measurement: 'kWh',
        value_template:
            "{{ states('sensor.production_tarif_1_daily')|float + states('sensor.production_tarif_2_daily')|float }}",
    },
    power_production_monthly: {
        friendly_name: 'Opbrengst deze maand',
        unit_of_measurement: 'kWh',
        value_template:
            "{{ states('sensor.production_tarif_1_monthly')|float + states('sensor.production_tarif_2_monthly')|float }}",
    },
    power_result_daily: {
        friendly_name: 'Resultaat vandaag',
        unit_of_measurement: 'kWh',
        value_template:
            "{{ states('sensor.power_production_daily')|float - states('sensor.power_consumption_daily')|float }}",
    },
    power_result_monthly: {
        friendly_name: 'Resultaat deze maand',
        unit_of_measurement: 'kWh',
        value_template:
            "{{ states('sensor.power_production_monthly')|float - states('sensor.power_consumption_monthly')|float }}",
    },
    power_cost_daily: {
        friendly_name: 'Energie kost vandaag',
        unit_of_measurement: '€',
        value_template: `{{ ((states('sensor.power_production_daily')|float * ${tarifPowerProduction}) - (states('sensor.power_consumption_daily')|float * ${tarifPowerConsumption})) * -1 }}`,
    },
    power_cost_monthly: {
        friendly_name: 'Energie kost deze maand',
        unit_of_measurement: '€',
        value_template: `{{ ((states('sensor.power_production_monthly')|float * ${tarifPowerProduction}) - (states('sensor.power_consumption_monthly')|float * ${tarifPowerConsumption})) * -1 }}`,
    },
    gas_cost_monthly: {
        friendly_name: 'Gas kosten deze maand',
        unit_of_measurement: '€',
        value_template: `{{ states('sensor.consumption_gas_monthly')|float * ${tarifGasConsumption} }}`,
    },
}

const sensors = [
    cpuTemperature,
    // pvoutput,
    dsmr,
    intexSensor,
    {
        platform: 'template',
        sensors: {
            // power_consumption: {
            //     value_template:
            //         '{% if is_state_attr("sensor.pvoutput", "power_consumption", "NaN") %}0{% else %}{{ state_attr("sensor.pvoutput", "power_consumption") }}{% endif %}',
            //     friendly_name: 'Using',
            //     unit_of_measurement: 'Watt',
            // },
            // energy_consumption: {
            //     value_template:
            //         '{{ "%0.1f"|format(state_attr("sensor.pvoutput", "energy_consumption")|float/1000) }}',
            //     friendly_name: 'Used',
            //     unit_of_measurement: 'kWh',
            // },
            // power_generation: {
            //     value_template:
            //         '{% if is_state_attr("sensor.pvoutput", "power_generation", "NaN") %}0{% else %}{{ state_attr("sensor.pvoutput", "power_generation") }}{% endif %}',
            //     friendly_name: 'Generating',
            //     unit_of_measurement: 'Watt',
            // },
            // energy_generation: {
            //     value_template:
            //         '{% if is_state_attr("sensor.pvoutput", "energy_generation", "NaN") %}0{% else %}{{ "%0.2f"|format(state_attr("sensor.pvoutput", "energy_generation")|float/1000) }}{% endif %}',
            //     friendly_name: 'Generated',
            //     unit_of_measurement: 'kWh',
            // },
            power_generation_vs_consumption: {
                value_template:
                    '{% if states("sensor.power_production") | float > 0 %}{{ states("sensor.power_production") | float }}{% else %}{{ states("sensor.power_consumption_2") | float * -1 }}{% endif %}',
                friendly_name: 'IN vs OUT',
                unit_of_measurement: 'kW',
            },
            calc_current_generation: {
                value_template: '{{ states("sensor.sma_ac_watt") | float }}',
                friendly_name: 'Current Generation',
                unit_of_measurement: 'W',
            },
            calc_power_out: {
                value_template:
                    '{{ states("sensor.power_generation_vs_consumption") | float * 1000 }}',
                friendly_name: 'Current Power OUT',
                unit_of_measurement: 'W',
            },
            calc_current_consumption: {
                value_template:
                    '{{ (states("sensor.calc_current_generation") | float) - (states("sensor.calc_power_out") | float) }}',
                friendly_name: 'Current Consumption',
                unit_of_measurement: 'W',
            },
            twcmanager_9177_state_friendly: {
                value_template: generateTwcmanagerStateFriendly(),
                friendly_name: 'TWC State',
            },
            ...powerGasConsumptionSensors,
        },
    } as TemplateSensor,
]

export default sensors
