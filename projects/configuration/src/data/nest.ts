import { Nest } from '../models'

const nest: Nest = {
    client_id: process.env['NEST_CLIENT_ID'],
    client_secret: process.env['NEST_CLIENT_SECRET'],
    project_id: '1db1c8ac-93ee-413f-b1a9-62dd634ce2e0',
    subscriber_id:
        'projects/mercurial-datum-301916/subscriptions/homeassistant',
}

export default nest
