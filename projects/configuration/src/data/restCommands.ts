import { RestCommand, RestCommandCollection } from '../models'

const authorizationToken = Buffer.from(
    `admin:${process.env['INTEX_REST_PASSWORD']}`,
).toString('base64')

const baseUrl = 'http://192.168.12.116:8080'
const baseCommand: Pick<
    RestCommand,
    'headers' | 'content_type' | 'method' | 'verify_ssl'
> = {
    headers: {
        'User-Agent': 'home-assistant',
        Authorization: `Basic ${authorizationToken}`,
    },
    content_type: 'application/json',
    method: 'POST',
    verify_ssl: false,
}

const getCommand = (path: string, payload = ''): RestCommand => {
    return {
        ...baseCommand,
        payload,
        url: `${baseUrl}${path}`,
    }
}

const restCommands: RestCommandCollection = {
    intex_spa_service_increase: getCommand('/increase'),
    intex_spa_service_decrease: getCommand('/decrease'),
    intex_spa_service_reset: getCommand('/reset'),
    intex_spa_service_turn_on: getCommand('/on'),
    intex_spa_service_turn_off: getCommand('/off'),
    intex_spa_service_set_min_temperature: getCommand(
        '/config/minTemperature',
        '{"minTemperature": "{{ temperature }}"}',
    ),
    intex_spa_service_set_max_temperature: getCommand(
        '/config/maxTemperature',
        '{"maxTemperature": "{{ temperature }}"}',
    ),
}

export default restCommands
