import { Scene } from '../models/scenes'

export const scene: Scene[] = [
    {
        name: 'Slaapkamer Lenn - WIT licht',
        entities: {
            'light.lenn_1': {
                state: 'on',
                brightness: 254,
                color_mode: 'color_temp',
                color_temp: 333,
            },
            'light.lenn_2': {
                state: 'on',
                brightness: 254,
                color_mode: 'color_temp',
                color_temp: 333,
            },
        },
    },
    {
        name: 'Slaapkamer Lenn - RGB licht',
        entities: {
            'light.lenn_1': {
                state: 'on',
                brightness: 254,
                effect: 'Gradual Change',
            },
            'light.lenn_2': {
                state: 'on',
                brightness: 254,
                effect: 'Gradual Change',
            },
        },
    },
]
