import { HomeConnect } from '../models'

const homeConnect: HomeConnect = {
    client_id: process.env['HOME_CONNECT_CLIENT_ID'],
    client_secret: process.env['HOME_CONNECT_CLIENT_SECRET'],
}

export default homeConnect
