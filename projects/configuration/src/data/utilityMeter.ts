import { UtilityMeter } from '../models/utilityMeter'

const utility_meter: UtilityMeter = {
    consumption_tarif_1_daily: {
        source: 'sensor.energy_consumption_tarif_1',
        cycle: 'daily',
    },
    consumption_tarif_1_monthly: {
        source: 'sensor.energy_consumption_tarif_1',
        cycle: 'monthly',
    },
    consumption_tarif_2_daily: {
        source: 'sensor.energy_consumption_tarif_2',
        cycle: 'daily',
    },
    consumption_tarif_2_monthly: {
        source: 'sensor.energy_consumption_tarif_2',
        cycle: 'monthly',
    },
    production_tarif_1_monthly: {
        source: 'sensor.energy_production_tarif_1',
        cycle: 'monthly',
    },
    production_tarif_1_daily: {
        source: 'sensor.energy_production_tarif_1',
        cycle: 'daily',
    },
    production_tarif_2_monthly: {
        source: 'sensor.energy_production_tarif_2',
        cycle: 'monthly',
    },
    production_tarif_2_daily: {
        source: 'sensor.energy_production_tarif_2',
        cycle: 'daily',
    },
    consumption_gas_monthly: {
        source: 'sensor.gas_consumption',
        cycle: 'monthly',
    },
    consumption_gas_daily: {
        source: 'sensor.gas_consumption',
        cycle: 'daily',
    },
}

export default utility_meter
