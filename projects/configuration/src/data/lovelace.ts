import { LoveLace } from '../models'

const lovelace: LoveLace = {
    mode: 'yaml',
    resources: [
        {
            type: 'module',
            url: '/local/kiosk-mode-card.js?t=' + Date.now(),
        },
        {
            type: 'module',
            url: '/local/clock-card.js?t=' + Date.now(),
        },
        // {
        //     type: 'module',
        //     url: '/local/bring-shopping-list-card.js?t=' + Date.now(),
        // },
        {
            type: 'module',
            url: '/local/intex-spa-card.js?t=' + Date.now(),
        },
        {
            type: 'module',
            url: '/local/layout-card.js?t=' + Date.now(),
        },
        {
            type: 'module',
            url: '/local/mushroom.js?t=' + Date.now(),
        },
    ],
    dashboards: {
        'ui-keuken': {
            mode: 'yaml',
            filename: 'keuken.yaml',
            title: 'Keuken',
            icon: 'mdi:table-chair',
            show_in_sidebar: true,
            require_admin: false,
        },
        'ui-energie': {
            mode: 'yaml',
            filename: 'energie.yaml',
            title: 'Energie',
            icon: 'mdi:home-lightning-bolt',
            show_in_sidebar: true,
            require_admin: false,
        },
        'ui-wout': {
            mode: 'yaml',
            filename: 'wout.yaml',
            title: 'Wout',
            icon: 'mdi:account',
            show_in_sidebar: true,
            require_admin: false,
        },
        'ui-mobile': {
            mode: 'yaml',
            filename: 'mobile.yaml',
            title: 'Mobile',
            icon: 'mdi:account',
            show_in_sidebar: true,
            require_admin: false,
        },
    },
}

export default lovelace
