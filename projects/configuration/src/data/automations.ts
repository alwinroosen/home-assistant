import { Automation, AutomationCollection, Device } from '../models'
import { luchtontvochtigerWapenkamer } from '../devices/luchtontvochtiger_wapenkamer'
import { luchtontvochtigerWerkplaats } from '../devices/luchtontvochtiger_werkplaats'
import { luchtontvochtigerMagazijn } from '../devices/luchtontvochtiger_magazijn'
import { luchtontvochtigerCreativeRoom } from '../devices/luchtontvochtiger_creative_room'
import { jacuzzi } from '../devices/jacuzzi'

const notifyService = 'notify.mobile_app_sm_g998b'

const luchtOntvochtigerAan = (device: Device): Omit<Automation, 'id'> => {
    return {
        alias: device.alias + ' AAN',
        description: 'Wanneer boven 50%, zet aan',
        trigger: {
            platform: 'state',
            entity_id: device.entities.humidity_above_50.entity_id,
            to: 'on',
        },
        condition: [
            {
                condition: 'state',
                entity_id: 'sun.sun',
                state: 'above_horizon',
            },
        ],
        action: [
            {
                type: 'turn_on',
                device_id: device.device_id,
                entity_id: device.entities.switch.entity_id,
                domain: 'switch',
            },
            {
                service: notifyService,
                data: {
                    message: device.alias + ' AAN',
                },
            },
        ],
        mode: 'single',
    }
}

const luchtOntvochtigerUit = (device: Device): Omit<Automation, 'id'> => {
    return {
        alias: device.alias + ' UIT',
        description: 'Wanneer onder 50%, zet uit',
        trigger: {
            platform: 'state',
            entity_id: device.entities.humidity_above_50.entity_id,
            to: 'off',
        },
        condition: [],
        action: [
            {
                type: 'turn_off',
                device_id: device.device_id,
                entity_id: device.entities.switch.entity_id,
                domain: 'switch',
            },
            {
                service: notifyService,
                data: {
                    message: device.alias + ' UIT',
                },
            },
        ],
        mode: 'single',
    }
}

const luchtOntvochtigerAanUit = (
    aanId: string,
    uitId: string,
    device: Device,
): Automation[] => {
    return [
        {
            id: aanId,
            ...luchtOntvochtigerAan(device),
        },
        {
            id: uitId,
            ...luchtOntvochtigerUit(device),
        },
    ]
}

const automations: AutomationCollection = [
    {
        id: '1602005297735',
        alias: 'Zet luchtontvochtigers uit om 21u',
        description: '',
        trigger: {
            platform: 'time',
            at: '21:00',
        },
        condition: [],
        action: [
            luchtontvochtigerCreativeRoom,
            luchtontvochtigerMagazijn,
            luchtontvochtigerWerkplaats,
            luchtontvochtigerWapenkamer,
        ].map((device) => ({
            type: 'turn_off',
            device_id: device.device_id,
            entity_id: device.entities.switch.entity_id,
            domain: 'switch',
        })),
        mode: 'single',
    },
    {
        id: '1602005380857',
        alias: 'Zet luchtontvochtigers aan om 9u',
        description: '',
        trigger: {
            platform: 'time',
            at: '09:00',
        },
        condition: [],
        action: [
            luchtontvochtigerCreativeRoom,
            luchtontvochtigerMagazijn,
            luchtontvochtigerWerkplaats,
            luchtontvochtigerWapenkamer,
        ].map((device) => ({
            type: 'turn_on',
            device_id: device.device_id,
            entity_id: device.entities.switch.entity_id,
            domain: 'switch',
        })),
        mode: 'single',
    },
    ...luchtOntvochtigerAanUit(
        '1610826549060',
        '1610826549061',
        luchtontvochtigerCreativeRoom,
    ),
    ...luchtOntvochtigerAanUit(
        '1610826549062',
        '1610826549063',
        luchtontvochtigerMagazijn,
    ),
    ...luchtOntvochtigerAanUit(
        '1610826549064',
        '1610826549065',
        luchtontvochtigerWerkplaats,
    ),
    ...luchtOntvochtigerAanUit(
        '1610826549066',
        '1610826549067',
        luchtontvochtigerWapenkamer,
    ),
    {
        id: '123456789012',
        mode: 'single',
        alias: 'Notification wanneer jacuzzi heater aan gaat',
        description: '',
        condition: [],
        trigger: {
            platform: 'state',
            entity_id: jacuzzi.entities.jacuzzi_heater_on.entity_id,
            to: 'on',
        },
        action: [
            {
                service: notifyService,
                data: {
                    message: 'Jacuzzi verwarming AAN',
                },
            },
        ],
    },
    {
        id: '123456789013',
        mode: 'single',
        alias: 'Notification wanneer jacuzzi heater uit gaat',
        description: '',
        condition: [],
        trigger: {
            platform: 'state',
            entity_id: jacuzzi.entities.jacuzzi_heater_on.entity_id,
            to: 'off',
        },
        action: [
            {
                service: notifyService,
                data: {
                    message: 'Jacuzzi verwarming UIT',
                },
            },
        ],
    },
    {
        id: '123456789014',
        mode: 'single',
        alias: 'Notification wanneer jacuzzi aan gaat',
        description: '',
        condition: [],
        trigger: {
            platform: 'state',
            entity_id: jacuzzi.entities.jacuzzi_on.entity_id,
            to: 'on',
        },
        action: [
            {
                service: notifyService,
                data: {
                    message: 'Jacuzzi AAN',
                },
            },
        ],
    },
    {
        id: '123456789015',
        mode: 'single',
        alias: 'Notification wanneer jacuzzi uit gaat',
        description: '',
        condition: [],
        trigger: {
            platform: 'state',
            entity_id: jacuzzi.entities.jacuzzi_on.entity_id,
            to: 'off',
        },
        action: [
            {
                service: notifyService,
                data: {
                    message: 'Jacuzzi UIT',
                },
            },
        ],
    },
]

export default automations
