import { AutomationCollection } from '../models'

import { lightsLennAutomations } from './automations/lightsLenn'
import { lightsKelderAutomations } from './automations/lightsKelder'
import { luchtOntvochtigerAutomations } from './automations/luchtOntvochtiger'

const automations: AutomationCollection = [
    // ...jacuzziAutomations,
    ...lightsLennAutomations,
    ...lightsKelderAutomations,
    // ...luchtOntvochtigerAutomations,
]

export default automations
