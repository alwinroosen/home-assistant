import { CameraCollection } from '../models'

const cameras: CameraCollection = [
    {
        platform: 'generic',
        still_image_url:
            'https://unifi/app-assets/protect/1138442a03390a81becac21bfa2789ef.png',
        stream_source: 'rtsp://192.168.0.1:7447/JSJj8kJTmgwW2KRi',
        name: 'Achtertuin',
        framerate: 16,
    },
    {
        platform: 'generic',
        still_image_url:
            'https://unifi/app-assets/protect/1138442a03390a81becac21bfa2789ef.png',
        stream_source: 'rtsp://192.168.0.1:7447/O4fLdRzxQA2rSisd',
        name: 'Terras',
        framerate: 16,
    },
    {
        platform: 'generic',
        still_image_url:
            'https://unifi/app-assets/protect/1138442a03390a81becac21bfa2789ef.png',
        stream_source: 'rtsp://192.168.0.1:7447/YruEUDhKNZyzQDOm',
        name: 'Voortuin',
        framerate: 16,
    },
    {
        platform: 'generic',
        still_image_url:
            'https://unifi/app-assets/protect/1138442a03390a81becac21bfa2789ef.png',
        stream_source: 'rtsp://192.168.0.1:7447/co7pp0PqvE30Dp7w',
        name: 'Zijkant',
        framerate: 16,
    },
]

export default cameras
