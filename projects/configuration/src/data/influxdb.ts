import { Influxdb } from '../models'

const influxdb: Influxdb = {
    api_version: 2,
    ssl: true,
    host: 'eu-central-1-1.aws.cloud2.influxdata.com',
    port: 443,
    token: process.env['INFLUXDB_TOKEN'],
    organization: 'alwin.roosen@ardynamics.eu',
    bucket: 'HomeAssistant',
}

export default influxdb
