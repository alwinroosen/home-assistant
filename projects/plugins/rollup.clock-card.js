import typescript from '@rollup/plugin-typescript'
import json from '@rollup/plugin-json'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
// eslint-disable-next-line @typescript-eslint/no-var-requires,no-undef
const tsConfig = require('./tsconfig.json')

export default {
    input: './src/clock-card.ts',
    output: {
        dir: '../../dist/www',
        format: 'iife',
    },
    external: [],
    plugins: [
        json(),
        typescript(tsConfig.compilerOptions),
        resolve(),
        commonjs(),
    ],
}
