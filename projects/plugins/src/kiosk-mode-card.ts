import { customElement, LitElement, property } from 'lit-element'
import * as pjson from '../../../package.json'
import { HomeAssistant } from 'custom-card-helpers'

declare global {
    interface Window {
        Bugsnag: any
    }
}

interface KioskModeCardConfig {
    type: string
    entity?: string
    name?: string
    icon?: string
}

console.info(
    `%c  KIOSK-MODE-CARD  \n%c Version ${pjson.version} `,
    'color: orange; font-weight: bold; background: black',
    'color: white; font-weight: bold; background: dimgray',
)

// This puts your card into the UI card picker dialog
;(window as any).customCards = (window as any).customCards || []
;(window as any).customCards.push({
    type: 'kiosk-mode-card',
    name: 'Kiosk Mode Card',
    description: 'Enable kiosk mode',
})

@customElement('kiosk-mode-card')
export class KioskModeCard extends LitElement {
    @property() private _hass?: HomeAssistant
    @property() private _config?: KioskModeCardConfig

    private _kiosk_mode_card_initial_setup_complete = false

    public set hass(hass: HomeAssistant) {
        this._hass = hass
        if (!this._kiosk_mode_card_initial_setup_complete) {
            console.log(`KIOSK-MODE-CARD INIT`, hass)
            this._kiosk_mode_card_initial_setup_complete = true
            this.enableKioskMode()
            this.loadBugSnag()
        }
    }

    public setConfig(config) {
        this._config = config
    }

    public configChanged(newConfig) {
        const event = new Event('config-changed', {
            bubbles: true,
            composed: true,
        })
        // @ts-ignore
        event.detail = { config: newConfig }
        this.dispatchEvent(event)
    }

    enableKioskMode() {
        if (window.location.href.includes('kiosk')) {
            setTimeout(function () {
                // document.addEventListener(
                //     'touchstart',
                //     function (event) {
                //         event.preventDefault()
                //     },
                //     { passive: false },
                // )
                try {
                    if (document.querySelector) {
                        const bodyElm = document.querySelector(
                            'body > home-assistant',
                        )
                        if (bodyElm) {
                            const shadowRoot = bodyElm.shadowRoot
                            if (shadowRoot) {
                                const mainElm: HTMLElement | null = shadowRoot.querySelector(
                                    'home-assistant-main',
                                )
                                if (mainElm) {
                                    mainElm.style.setProperty(
                                        '--app-drawer-width',
                                        '0',
                                    )

                                    const mainShadowRoot = mainElm.shadowRoot

                                    if (mainShadowRoot) {
                                        const headerPanel = mainShadowRoot.querySelector(
                                            'app-drawer-layout > partial-panel-resolver > ha-panel-lovelace',
                                        )
                                        if (headerPanel) {
                                            const headerPanelShadowRoot =
                                                headerPanel.shadowRoot
                                            if (headerPanelShadowRoot) {
                                                const headerRoot: HTMLElement | null = headerPanelShadowRoot.querySelector(
                                                    'hui-root',
                                                )
                                                if (headerRoot) {
                                                    const headerRootShadowRoot =
                                                        headerRoot.shadowRoot
                                                    if (headerRootShadowRoot) {
                                                        const headerToolbar: HTMLElement | null = headerRootShadowRoot.querySelector(
                                                            '#layout > app-header > app-toolbar',
                                                        )
                                                        if (headerToolbar) {
                                                            headerToolbar.style.display =
                                                                'none'
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        const drawer: HTMLElement | null = mainShadowRoot.querySelector(
                                            '#drawer',
                                        )

                                        if (drawer) {
                                            drawer.style.display = 'none'
                                        }

                                        window.dispatchEvent(
                                            new Event('resize'),
                                        )
                                    }
                                }
                            }
                        }
                    }
                } catch (e) {
                    console.log(e)
                }
            }, 200)
        }
    }

    loadBugSnag() {
        const scriptElement = document.createElement('script')
        scriptElement.type = 'text/javascript'
        scriptElement.src = '//d2wy8f7a9ursnm.cloudfront.net/v7/bugsnag.min.js'
        scriptElement.onload = () => {
            window.Bugsnag.start({ apiKey: '79ad7e8952f5f0eac8e206e7da1618f3' })
        }
        document.head.appendChild(scriptElement)
    }
}
