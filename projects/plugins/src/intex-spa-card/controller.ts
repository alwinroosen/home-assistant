import { HomeAssistant } from 'custom-card-helpers'

interface IController {
    turnOn: () => Promise<void>
    turnOff: () => Promise<void>
    reset: () => Promise<void>
}

type IControllerMethods = keyof IController

export class Controller implements IController {
    private readonly hass: HomeAssistant

    private _onButtonTimer?: NodeJS.Timer
    private _offButtonTimer?: NodeJS.Timer
    private _onButtonDisabled = false
    private _offButtonDisabled = false

    constructor(hass: HomeAssistant) {
        this.hass = hass
    }

    async turnOn(): Promise<void> {
        if (this._onButtonDisabled) {
            return
        }
        this._onButtonDisabled = true
        this._offButtonDisabled = true
        this._onButtonTimer = setTimeout(() => {
            this._offButtonDisabled = false
        }, 5000)
        return this.hass.callService(
            'rest_command',
            'intex_spa_service_turn_on',
            {
                entity_id: 'sensor.intex_spa_state',
            },
        )
    }

    async turnOff(): Promise<void> {
        if (this._offButtonDisabled) {
            return
        }
        this._onButtonDisabled = true
        this._offButtonDisabled = true
        this._offButtonTimer = setTimeout(() => {
            this._onButtonDisabled = false
        }, 5000)
        return this.hass.callService(
            'rest_command',
            'intex_spa_service_turn_off',
            {
                entity_id: 'sensor.intex_spa_state',
            },
        )
    }

    async reset(): Promise<void> {
        return this.hass.callService(
            'rest_command',
            'intex_spa_service_reset',
            {
                entity_id: 'sensor.intex_spa_state',
            },
        )
    }

    isAvailable(method: IControllerMethods): boolean {
        switch (method) {
            case 'turnOn':
                return !this._onButtonDisabled
            case 'turnOff':
                return !this._offButtonDisabled
            default:
                return true
        }
    }
}
