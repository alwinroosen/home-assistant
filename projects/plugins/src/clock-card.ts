import { customElement, LitElement, property, html } from 'lit-element'
import * as pjson from '../../../package.json'
import { HomeAssistant } from 'custom-card-helpers'

interface ClockCardConfig {
    type: string
    entity?: string
    name?: string
    icon?: string
}

console.info(
    `%c  CLOCK-CARD  \n%c Version ${pjson.version} `,
    'color: orange; font-weight: bold; background: black',
    'color: white; font-weight: bold; background: dimgray',
)

// This puts your card into the UI card picker dialog
;(window as any).customCards = (window as any).customCards || []
;(window as any).customCards.push({
    type: 'clock-card',
    name: 'Clock Card',
    description: 'Show a clock',
})

@customElement('clock-card')
export class ClockCard extends LitElement {
    @property() private _hass?: HomeAssistant
    @property() private _config?: ClockCardConfig

    private _clock_card_initial_setup_complete = false
    private _interval
    private _date
    private _time

    public set hass(hass: HomeAssistant) {
        this._hass = hass
        if (!this._clock_card_initial_setup_complete) {
            console.log(`CLOCK-CARD INIT`, hass)
            this._clock_card_initial_setup_complete = true
            this.updateClock()
            this._interval = setInterval(() => {
                this.updateClock()
            }, 60000)
        }
    }

    private updateClock() {
        const now = new Date()
        const day = now.getDate()
        const month = now.getMonth()
        const year = now.getFullYear()
        this._date =
            (day < 10 ? '0' : '') +
            day +
            '/' +
            (month < 10 ? '0' : '') +
            month +
            '/' +
            (year < 10 ? '0' : '') +
            year
        const hours = now.getHours()
        const minutes = now.getMinutes()
        this._time =
            (hours < 10 ? '0' : '') +
            hours +
            ':' +
            (minutes < 10 ? '0' : '') +
            minutes
    }

    public setConfig(config) {
        this._config = config
        console.log(`CLOCK-CARD set config`, config)
    }

    public configChanged(newConfig) {
        const event = new Event('config-changed', {
            bubbles: true,
            composed: true,
        })
        // @ts-ignore
        event.detail = { config: newConfig }
        console.log(`CLOCK-CARD configChanged`, newConfig, event)
        this.dispatchEvent(event)
    }

    renderStyle() {
        return html`
            <style>
                .clock-card-container {
                    padding: 1rem;
                    position: relative;
                }
                .clock-card-container > .card-header {
                    padding: 0;
                    line-height: inherit;
                }
                .clock-card-container > .card-content {
                    padding: 1.5rem 0 0 0;
                    text-align: center;
                }
                .clock-card-container > .card-content > h1 {
                    margin: 0;
                    font-size: 5rem;
                    line-height: 5rem;
                }
                .clock-card-container > .card-version {
                    position: absolute;
                    top: 5px;
                    right: 5px;
                    font-size: 1rem;
                }
            </style>
        `
    }

    render() {
        return html`${this.renderStyle()}
            <ha-card class="clock-card-container">
                <h1 class="card-header">${this._date}</h1>
                <div class="card-content">
                    <h1>${this._time}</h1>
                </div>
                <div class="card-version">${pjson.version}</div>
            </ha-card>`
    }
}
