#!/bin/bash
rollup -c rollup.clock-card.js
rollup -c rollup.kiosk-mode-card.js
rollup -c rollup.bring-shopping-list-card.js
rollup -c rollup.intex-spa-card.js
cp ./src/index.html ../../dist/www/index.html
cp ./src/layout-card.js ../../dist/www/layout-card.js
