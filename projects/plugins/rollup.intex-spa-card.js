import typescript from '@rollup/plugin-typescript'
import json from '@rollup/plugin-json'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
// eslint-disable-next-line @typescript-eslint/no-var-requires,no-undef
const tsConfig = require('./tsconfig.json')

export default [
    {
        input: 'src/intex-spa-card.ts',
        output: {
            dir: '../../dist/www',
            format: 'iife',
        },
        plugins: [
            json(),
            typescript(tsConfig.compilerOptions),
            resolve(),
            commonjs(),
        ],
    },
]
