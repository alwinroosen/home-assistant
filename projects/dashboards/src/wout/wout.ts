import { Dashboard } from '../utils/types'

export const wout: Dashboard = {
    name: 'wout',
    title: '',
    theme: 'Google - Dark',
    views: [
        {
            panel: false,
            badges: [
                { entity: 'person.wout_beersaerts' },
                { entity: 'binary_sensor.raam_wout_1' },
                { entity: 'binary_sensor.raam_wout_2' },
                { entity: 'binary_sensor.raam_wout_3' },
            ],
            cards: [
                {
                    type: 'light',
                    entity: 'light.slaapkamer_wout',
                },
                {
                    type: 'light',
                    entity: 'light.bureau_wout',
                },
                {
                    type: 'media-control',
                    entity: 'media_player.speaker_wout',
                },
                {
                    // @ts-ignore
                    show_name: true,
                    show_icon: true,
                    type: 'button',
                    tap_action: {
                        action: 'toggle',
                    },
                    entity: 'switch.shelly1_poort_kelder',
                    icon: 'mdi:garage-variant',
                    name: 'Poort Kelder',
                    show_state: false,
                    hold_action: {
                        action: 'none',
                    },
                },
            ],
        },
    ],
}
