import { config } from 'dotenv'

config()

import * as YAML from 'yaml'
import * as fs from 'fs'
import * as path from 'path'

import { Dashboards } from './keuken/utils/types'
import { keuken } from './keuken/keuken'

const buildDir = path.resolve('.', 'dist')

if (!fs.existsSync(buildDir)) {
    fs.mkdirSync(buildDir)
}

const dashboards: Dashboards = [keuken]

dashboards.forEach((dashboard) => {
    fs.writeFileSync(
        path.resolve(buildDir, `${dashboard.name}.yaml`),
        YAML.stringify(
            {
                title: dashboard.title,
                views: dashboard.views,
            },
            {
                schema: 'json',
            },
        ),
    )
})
