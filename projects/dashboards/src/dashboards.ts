import { config } from 'dotenv'

config()

import * as YAML from 'yaml'
import * as fs from 'fs'
import * as path from 'path'

import { Dashboards } from './utils/types'
import { energie } from './energie/energie'
import { keuken } from './keuken/keuken'
import { wout } from './wout/wout'
import { mobile } from './mobile/mobile'

const buildDir = path.resolve('.', 'dist')

if (!fs.existsSync(buildDir)) {
    fs.mkdirSync(buildDir)
}

const dashboards: Dashboards = [energie, keuken, wout, mobile]

dashboards.forEach((dashboard) => {
    fs.writeFileSync(
        path.resolve(buildDir, `${dashboard.name}.yaml`),
        YAML.stringify(
            {
                title: dashboard.title,
                views: dashboard.views,
            },
            {
                schema: 'json',
            },
        ),
    )
})
