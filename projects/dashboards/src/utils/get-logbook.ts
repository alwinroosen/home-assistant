import { Logbook } from './types'

type GetLogbook = Pick<Logbook, 'entities' | 'title'> & Partial<Logbook>

export const getLogbook = (config: GetLogbook): Logbook => {
    return {
        type: 'logbook',
        hours_to_show: 24,
        ...config,
    }
}
