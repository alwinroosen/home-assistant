import { Light, MushroomLight } from './types'

export const getLight = (
    config: Pick<Light, 'entity' | 'name'> & Partial<Light>,
): Light => {
    return {
        type: 'light',
        hold_action: {
            action: 'more-info',
        },
        ...config,
    }
}

export const getMushroomLight = (
    config: Pick<MushroomLight, 'entity' | 'name'> & Partial<MushroomLight>,
): MushroomLight => {
    return {
        type: 'custom:mushroom-light-card',
        fill_container: false,
        primary_info: 'name',
        secondary_info: 'state',
        use_light_color: false,
        show_brightness_control: true,
        show_color_temp_control: true,
        collapsible_controls: true,
        ...config,
    }
}
