import { Thermostat } from './types'

type GetThermostat = Pick<Thermostat, 'entity'> & Partial<Thermostat>

export const getThermostat = (config: GetThermostat): Thermostat => {
    return {
        type: 'thermostat',
        ...config,
    }
}
