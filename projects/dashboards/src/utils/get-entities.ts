import { Entities } from './types'

type GetEntities = Pick<Entities, 'entities' | 'title'> & Partial<Entities>

export const getEntities = (config: GetEntities): Entities => {
    return {
        type: 'entities',
        ...config,
    }
}
