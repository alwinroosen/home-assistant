import { VerticalStack } from './types'

type GetVerticalStack = Pick<VerticalStack, 'cards'> & Partial<VerticalStack>

export const getVerticalStack = (config: GetVerticalStack): VerticalStack => {
    return {
        type: 'vertical-stack',
        ...config,
    }
}
