import { Glance } from './types'

type GetGlance = Pick<Glance, 'entities'> & Partial<Glance>

export const getGlance = (config: GetGlance): Glance => {
    return {
        type: 'glance',
        show_icon: false,
        ...config,
    }
}
