
import { StatisticsGraph } from './types'

type GetStatisticsGraph = Pick<StatisticsGraph, 'title' | 'entities'> & Partial<StatisticsGraph>

export const getStatisticsGraph = (config: GetStatisticsGraph): StatisticsGraph => {
  return {
    type: 'statistics-graph',
    ...config,
  }
}
