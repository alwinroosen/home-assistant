import { Grid } from './types'

type GetGrid = Pick<Grid, 'cards'> & Partial<Grid>

export const getGrid = (config: GetGrid): Grid => {
    return {
        type: 'grid',
        columns: 4,
        square: true,
        ...config,
    }
}
