import { Gauge } from './types'

type GetGauge = Pick<Gauge, 'entity' | 'name'> & Partial<Gauge>

export const getGauge = (config: GetGauge): Gauge => {
    return {
        type: 'gauge',
        max: 100,
        min: 0,
        ...config,
    }
}
