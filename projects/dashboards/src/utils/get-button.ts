import { Button } from './types'

type GetButton = Pick<Button, 'entity'> & Partial<Button>

export const getButton = (config: GetButton): Button => {
    return {
        type: 'button',
        show_name: false,
        show_state: false,
        icon_height: '2rem',
        ...config,
    }
}
