import { getLogbook } from '../utils/get-logbook'

export const logbook = getLogbook({
    title: 'Logbook',
    entities: [
        'switch.37685021bcddc28b9c68',
        'switch.40605414bcddc28b9bb5',
        'switch.80020737b4e62d42508b',
        'switch.80020737bcddc28b9b33',
        'automation.zet_luchtontvochtigers_aan_om_8u',
        'automation.zet_luchtontvochtigers_uit_om_22u',
        'climate.living_room',
        'sensor.twcmanager_all_total_amps_in_use',
        'sensor.twcmanager_9177_cars_charging',
    ],
})
