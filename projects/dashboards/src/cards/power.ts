import { VerticalStack } from '../utils/types'
import { getGauge } from '../utils/get-gauge'
import { getVerticalStack } from '../utils/get-vertical-stack'
import { getHorizontalStack } from '../utils/get-horizontal-stack'

const powerGenerationVsConsumption = getGauge({
    entity: 'sensor.power_generation_vs_consumption',
    name: 'Status',
    min: -20,
    max: 10,
    severity: {
        green: 0,
        yellow: -2.5,
        red: -12.5,
    },
})

const currentGeneration = getGauge({
    entity: 'sensor.calc_current_generation',
    name: 'Opbrengst',
    max: 10000,
})

const currentConsumption = getGauge({
    entity: 'sensor.calc_current_consumption',
    name: 'Verbruik',
    max: 20000,
})

const powerDetails = getHorizontalStack({
    cards: [currentGeneration, currentConsumption],
})

export const power: VerticalStack = getVerticalStack({
    cards: [powerGenerationVsConsumption, powerDetails],
})
