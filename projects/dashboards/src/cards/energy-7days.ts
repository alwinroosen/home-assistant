export const energy7Days = {
    chart_type: 'bar',
    period: 'day',
    days_to_show: 7,
    type: 'statistics-graph',
    entities: [
        {
            entity: 'sensor.power_consumption_2',
            name: 'Verbruik',
        },
        {
            entity: 'sensor.power_production',
            name: 'Opbrengst',
        },
    ],
    stat_types: ['mean'],
}
