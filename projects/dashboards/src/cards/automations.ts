import { getEntity } from '../utils/get-entity'
import { getHorizontalStack } from '../utils/get-horizontal-stack'

export const automations = getHorizontalStack({
    type: 'horizontal-stack',
    cards: [
        getEntity({
            entity: 'input_select.jacuzzi_auto',
            name: 'Jacuzzi',
        }),
        getEntity({
            entity: 'input_select.airco_auto',
            name: 'Airco',
        }),
        getEntity({
            entity: 'input_select.audi_etron_auto',
            name: 'Audi Etron',
        }),
    ],
})
