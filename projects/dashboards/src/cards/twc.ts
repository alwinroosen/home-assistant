import { getEntities } from '../utils/get-entities'

export const twc = getEntities({
    title: 'Tesla',
    entities: [
        {
            entity: 'sensor.twcmanager_9177_power',
            name: 'Huidig verbruik',
        },
        {
            entity: 'sensor.twcmanager_9177_state_friendly',
            name: 'Status',
        },
        {
            entity: 'sensor.twcmanager_9177_lifetime_kwh',
            name: 'Totaal verbruik',
        },
    ],
})
