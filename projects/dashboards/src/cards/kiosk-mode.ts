import { getCard } from '../utils/get-card'

export const kioskMode = getCard({
    entity: 'device_tracker.raspberrypi',
    type: 'custom:kiosk-mode-card',
})
