import { getButton } from '../utils/get-button'
import { getGauge } from '../utils/get-gauge'
import { getGrid } from '../utils/get-grid'
import { getVerticalStack } from '../utils/get-vertical-stack'

export const shellyPlugs = getGrid({
    cards: [
        getVerticalStack({
            cards: [
                getButton({
                    entity: 'switch.shelly_plug_lo_creative_room',
                }),
                getGauge({
                    entity: 'sensor.shelly_plug_lo_creative_room_power',
                    name: 'Crea.Room',
                    severity: {
                        red: 350,
                        yellow: 30,
                        green: 0,
                    },
                }),
            ],
        }),
        getVerticalStack({
            cards: [
                getButton({
                    entity: 'switch.shelly_plug_lo_studio',
                }),
                getGauge({
                    entity: 'sensor.shelly_plug_lo_studio_power',
                    name: 'Studio',
                    severity: {
                        red: 200,
                        yellow: 30,
                        green: 0,
                    },
                }),
            ],
        }),
        getVerticalStack({
            cards: [
                getButton({
                    entity: 'switch.shelly_plug_lo_magazijn',
                }),
                getGauge({
                    entity: 'sensor.shelly_plug_lo_magazijn_power',
                    name: 'Magazijn',
                    severity: {
                        red: 350,
                        yellow: 30,
                        green: 0,
                    },
                }),
            ],
        }),
        getVerticalStack({
            cards: [
                getButton({
                    entity: 'switch.shelly_plug_lo_werkplaats',
                }),
                getGauge({
                    entity: 'sensor.shelly_plug_lo_werkplaats_power',
                    name: 'Werkplaats',
                    severity: {
                        red: 350,
                        yellow: 30,
                        green: 0,
                    },
                }),
            ],
        }),
        getVerticalStack({
            cards: [
                getButton({
                    entity: 'switch.shelly_plug_slaapkamer_airco',
                }),
                getGauge({
                    entity: 'sensor.shelly_plug_slaapkamer_airco_power',
                    name: 'Airco',
                    severity: {
                        red: 500,
                        yellow: 30,
                        green: 0,
                    },
                }),
            ],
        }),
        getVerticalStack({
            cards: [
                getButton({
                    entity: 'switch.shelly_plug_wout',
                }),
                getGauge({
                    entity: 'sensor.shelly_plug_wout_power',
                    name: 'Wout',
                    severity: {
                        red: 250,
                        yellow: 50,
                        green: 0,
                    },
                }),
            ],
        }),
    ],
})
