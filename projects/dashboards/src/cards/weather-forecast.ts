import { Card } from '../utils/types'

export const weatherForecast: Card = {
    entity: 'weather.verdaelstraat_16',
    type: 'weather-forecast',
}
