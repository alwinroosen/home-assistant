import { getButton } from '../utils/get-button'
import { getGauge } from '../utils/get-gauge'
import { getEntity } from '../utils/get-entity'
import { getGrid } from '../utils/get-grid'
import { getVerticalStack } from '../utils/get-vertical-stack'
import { Gauge } from '../utils/types'

const defaultGaugeSeverity: Pick<Gauge, 'severity'> = {
    severity: {
        green: 0,
        red: 60,
        yellow: 50,
    },
}

export const luchtontvochtigers = getGrid({
    cards: [
        getGauge({
            entity: 'sensor.creative_room_humidity',
            name: 'Crea.Room',
            ...defaultGaugeSeverity,
        }),
        getGauge({
            entity: 'sensor.wapenkamer_humidity',
            name: 'Studio',
            ...defaultGaugeSeverity,
        }),
        getGauge({
            entity: 'sensor.magazijn_humidity',
            name: 'Magazijn',
            ...defaultGaugeSeverity,
        }),
        getGauge({
            entity: 'sensor.workshop_humidity',
            name: 'Werkplaats',
            ...defaultGaugeSeverity,
        }),
    ],
})
