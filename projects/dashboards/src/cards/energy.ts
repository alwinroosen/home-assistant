import { getEntities } from '../utils/get-entities'

const dayYield = {
    entity: 'sensor.sma_day_yield',
    name: 'Opbrengst vandaag',
}

const totalYield = {
    entity: 'sensor.sma_total_yield',
    name: 'Totale opbrengst',
}

const gasConsumptionMonthly = {
    entity: 'sensor.consumption_gas_monthly',
    name: 'Gas deze maand',
}

const energyConsumptionDaily = {
    entity: 'sensor.power_consumption_daily',
    name: 'Verbruik vandaag',
}

const energyConsumptionMonthly = {
    entity: 'sensor.power_consumption_monthly',
    name: 'Verbruik deze maand',
}

const energyProductionDaily = {
    entity: 'sensor.power_production_daily',
    name: 'Opbrengst vandaag',
}

const energyProductionMonthly = {
    entity: 'sensor.power_production_monthly',
    name: 'Opbrengst deze maand',
}

const energyResultDaily = {
    entity: 'sensor.power_result_daily',
    name: 'Resultaat vandaag',
}

const energyResultMonthly = {
    entity: 'sensor.power_result_monthly',
    name: 'Resultaat deze maand',
}

const energyCostDaily = {
    entity: 'sensor.power_cost_daily',
    name: 'Energiekosten vandaag',
}

const energyCostMonthly = {
    entity: 'sensor.power_cost_monthly',
    name: 'Energiekosten deze maand',
}

const gasCostMonthly = {
    entity: 'sensor.gas_cost_monthly',
    name: 'Gas kosten deze maand',
}

export const energy = getEntities({
    entities: [
        dayYield,
        totalYield,
        gasConsumptionMonthly,
        energyConsumptionDaily,
        energyConsumptionMonthly,
        energyProductionDaily,
        energyProductionMonthly,
        energyResultDaily,
        energyResultMonthly,
        energyCostDaily,
        energyCostMonthly,
        gasCostMonthly,
    ],
    title: 'Energie',
})
