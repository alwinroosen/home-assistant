import { getThermostat } from '../utils/get-thermostat'

export const nest = getThermostat({
    entity: 'climate.living_room',
    name: 'Verwarming',
})
