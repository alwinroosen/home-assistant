import { getMushroomLight } from '../utils/get-light'
import { getHorizontalStack } from '../utils/get-horizontal-stack'
import { getVerticalStack } from '../utils/get-vertical-stack'

const keuken = getMushroomLight({
    entity: 'light.keuken',
    name: 'Keuken',
})

const eetkamer = getMushroomLight({
    entity: 'light.eetkamer',
    name: 'Eetkamer',
})

const leefruimte = getMushroomLight({
    entity: 'light.living_room',
    name: 'Living',
})

export const lights = getVerticalStack({
    cards: [keuken, eetkamer, leefruimte],
})
