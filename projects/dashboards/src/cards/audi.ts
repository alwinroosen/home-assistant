export const audi = {
    type: 'picture-elements',
    elements: [
        {
            type: 'state-label',
            entity: 'sensor.audi_e_tron_primary_engine_range',
            style: {
                top: '10%',
                left: '12%',
                'font-size': '20px',
                'font-weight': 'bold',
                'text-align': 'left',
            },
        },
        {
            type: 'state-label',
            entity: 'sensor.audi_e_tron_state_of_charge',
            style: {
                top: '10%',
                right: '-5%',
                'font-size': '20px',
                'font-weight': 'bold',
                'text-align': 'right',
            },
        },
        {
            type: 'icon',
            icon: 'mdi:lock',
            entity: 'lock.audi_e_tron_door_lock',
            tap_action: {
                action: 'call-service',
                service: 'audiconnect.execute_vehicle_action',
                data: {
                    vin: 'WAUZZZGE5PB024793',
                    action: 'unlock',
                },
            },
            style: {
                color: 'white',
                left: '10%',
                top: '86%',
            },
        },
        {
            type: 'state-label',
            entity: 'lock.audi_e_tron_door_lock',
            style: {
                color: 'white',
                left: '10%',
                top: '95%',
            },
        },
        {
            type: 'icon',
            icon: 'mdi:car-door',
            entity: 'sensor.audi_e_tron_doors_trunk_state',
            tap_action: 'more_info',
            style: {
                color: 'white',
                left: '26%',
                top: '86%',
            },
        },
        {
            type: 'state-label',
            entity: 'sensor.audi_e_tron_doors_trunk_state',
            style: {
                color: 'white',
                left: '26%',
                top: '95%',
            },
        },
        {
            type: 'icon',
            icon: 'mdi:battery-charging',
            entity: 'sensor.audi_e_tron_charging_state',
            tap_action: 'more_info',
            style: {
                color: 'white',
                left: '42%',
                top: '86%',
            },
        },
        {
            type: 'state-label',
            entity: 'sensor.audi_e_tron_charging_state',
            style: {
                color: 'white',
                left: '42%',
                top: '95%',
            },
        },
        {
            type: 'icon',
            icon: 'mdi:fan',
            entity: 'sensor.audi_e_tron_climatisation_state',
            tap_action: {
                action: 'call-service',
                service: 'audiconnect.execute_vehicle_action',
                data: {
                    vin: 'WAUZZZGE5PB024793',
                    action: 'start_climatisation',
                },
            },
            style: {
                color: 'white',
                left: '58%',
                top: '86%',
            },
        },
        {
            type: 'state-label',
            entity: 'sensor.audi_e_tron_climatisation_state',
            style: {
                color: 'white',
                left: '58%',
                top: '95%',
            },
        },
        {
            type: 'icon',
            icon: 'mdi:room-service-outline',
            entity: 'sensor.audi_e_tron_service_inspection_time',
            tap_action: 'more_info',
            style: {
                color: 'white',
                left: '74%',
                top: '86%',
            },
        },
        {
            type: 'state-label',
            entity: 'sensor.audi_e_tron_service_inspection_time',
            style: {
                color: 'white',
                left: '74%',
                top: '95%',
            },
        },
        {
            type: 'icon',
            icon: 'mdi:speedometer',
            entity: 'sensor.audi_e_tron_mileage',
            tap_action: 'more_info',
            style: {
                color: 'white',
                left: '90%',
                top: '86%',
            },
        },
        {
            type: 'state-label',
            entity: 'sensor.audi_e_tron_mileage',
            style: {
                color: 'white',
                left: '90%',
                top: '95%',
            },
        },
    ],
    image:
        'https://mediaservice.audi.com/media/fast/H4sIAAAAAAAAAI1W91OUyxIdsjiwqGRUJCjhkpGMZERAcFeQjASRnDMsGZWrZFYQliiC5KSSkYyIsESBS1gJkgQvSBQlvs9X7w9488Opmqmuqek-fU5P2Tqg8PUCZGZmqrePWRnt7aelSQEI8AAAkCDnpGaqZTvMvyjP3Jv73zHUAWQebvYkSMBDUmpHV2t7WxFkT0eCsvXxcncTsg3wEbL2c_ShQgKCYffiTVw_V0cDRfCkoLZ5RoIL7kE3ckVUKgljSPPdbfXYtjXiUZyvxRYFgPM02tghZW8jMp9P305n34zOzFKkA_jUK1PbnbwhpZrxSsOK6rXn-xxOqwAehJAs0VvOU_WVq5V-f8WunzBq0wJg_EGSySybtmmQye-J1pnIC90999YAbJTlIdnt-1nNJUK6T1nL9zwWdVkYuUGIPzrcNrq7j2Va6NR3O7W0rtsRQE-mBZxZh5lbuPCpL0Oo-FRMUj4TgMXRopZZeXI80uO7xq7nxBjcK46mASQPrqv1-IC65UreCcsn3rXdON8WhmRBjKsUL9sm-63P2BiVRUrvdSB2AOBqUIXVceSmkMJ6yEzbEC3vv7_9hQDsM-dd0SqP0PXWfor-RtWJ6fwHzQdg7424O7ea2re5dlpiDx9t8FhtPzIBEPWan16dcjJySQQ1QK2Uc6JMUJoD8Ap3nfn2j4ojY7Onsk6bfQP3mydyAOzkkM9lJqgM-wcTZGP2kiyWu3eskepI6ByQW8SNzeNXIvLsgiIsnNV8AByyj6oSPPIoumVtmnhJolQtWUSTE0CWBTUVs51EivCugNCtTO1D9aKEcwAanUiqBT_n_OyPDpN4XGbNkvGaXg7AUGZK6YfvsoXZnFG57PwUF2wy3ZMB1MftPZGtkBcWwFYDBf9ZnYLgqjQAW7aUB8rkDz_Ud5CEsZ7iaXnKxskRLno4k6PZ7exKuex4UwbeBl4WJp0CUMrmIrezUa3cA5lqo92GZvS_h-60AJI4ump1HprRF0YoZavUS2Q3vR6AANq_cjo8k5LYp2aMPpHCsKVs7cUivZOUUo8DyXjiPYUmzYlzDtbVJxxDAKrYMp-57WD0L_ohgfHN4ItcNny7O4CiBp_TtN8x75U0t-_kNqQtjLYKegOo4ZZs9atVAvb2HGOrFMgjhgyyOBDeBlbkhMQKseXsnFrBdrc9718yVQcwyEVDmDZab05ILi_nBCvCsEFwXQewAGVl159wLSCrnO7refWp1LTo9TEAud2_TYQdv0__-ZEiFcNdlJ338AUlgHbsiSV-tn4ovuEcivrRZ5ijAfwbRC17BtiivQ4WywiBWwThZzZcVPQIm0bvXHYx9n5TDGQh5pldcDUPi2sA0N9nMij6YNnZTx-yBHfHL3EKEo4BNFBf6Da-kP99-7WdJdWIotOLSUHkvVJjK4GYHfIE4e322lBMiiAobkN6h-M9jU-H2Ll-ZpMrpo3Pdi-Q0dyRAlCRXqHeozmiZ0Cp0Wjnwbl-cc2iIwCVXrosLRqRbN7V9_bT0rzzvf2MYyOAr5KiH_kM8soNU5udzXyXUdjsHiIKYCFm1fmegAEZzfVB0nj1mRMSI0lEhcs8n7irrDmV3sqVDZphvhb8pMNqAhiuZfN4a8b8q9bXEy8vlLxuFU_XLoDrASVrqU_4SJbkRDf3tnl1jjvnuQEcbqqWu9VBZDBP9FV7toij-eC-7AZgWxnt6-cJLAwJ15e-dBX_5n59Eh4O4MggbNh4b_g4JsPROzetJmIoVScKwISDZqo9z9CjDH8VTd9isprGLzuhAErwNmwtj1wh16FcEaj-kUF7B6-BMB9pfeoWu32Zl82Fl8a6UGQxjnXwJdJnTaTDPxTLjvUs646jcsa1N3Q6PwB4CUfrPz_rPGyQXDbOJcWhtkrrpgfglB-VzqTj2TnAJ9l339e_tlhlfgbAU2kUdp4u2dNq3IXG8jmNclhhliKAMSRNurn6z7m-_HVwkxk7RFYUwyMA4JOkGo4Hn5LrjA_t75tpXqxLstvLBlC5hln9S5yZ1-JVzXQ-qs3cQsXKYgB_7NMQY3RkPxI6622Wd9fiK7HEcgD1FvqzeUpS305zzKqe3Q2IG8v5ibC5er5oyk3YdKRvnHjXfPxs0r_52ygAux-pM9hxVXNMJFwMCvWawiTwyt5AOLbwPRo_XhJx26NyvzYx5BqkSS2N9ENl_dgJ27Lzqw1xaSKmmGRh0hTJTZi481rycVBX0HzXHfTZmu8MVE3jAGaET4pptt9QWVKOg60yqdRE6c-8AKZtGTLUjvqyX0mk0Xj0y06rSP4XHkA3a5b3DxrXmWwcYiOXXDGcSUp_nyL1NSil9hVZf4mW9JTxPoPOpavw3QBwoXJOMFtOPrC_EHWCr-yLD2fblEW8ekI731Ttkkiog5HfWe52TqkvKghvZoZH59ucIlXzH0SgSUcyccsahT8BpOUsPmibTq4PlDAUTSjXjdXXY3qC-O9G1AdaZnm2qGc1TXlVWR13Vap1AHSnsiXSbQdavEm_yjSVydFaxS91C8D6Z3iDCvNDV8a7PLX7l-p4y-OrSpGM9UMsWALRfIdCGga0X05DTlLlMwHsaaOzOD3qb1G2qavmu66w8pCtYhTR_EI7haMis2--p1_HL4qSlD7WeVsATV2xt-8TJZ8LEZbz8QUWkkqERWQgpxteenygxzTjX-LE-t5PviRsyT8V8V8v05EsaNSmY76Cw4uX5InwkzICqBbDgV61R7dM5npfhBWv-CeusSIqZN6h7OOKJKDuliyk2Bg7NhWPKCog3mfqIz3w0ofj0qMKSYZZR0F680h6AF0IytheFdGRuh48Q_u-HTnB0ByH6K015Y3-aqzntrPuqW1s5UtGDDcWwLcFQ6JCMgd_7_HzsqYxkLhYWUwiDnM9sPCvN9OhcQIL13s_DWftNjbYbwJY1851tDTj0PfxsJuK_TbOerqg2gqZvA5aU3PjYbp-n8SjHwWo4EznmJAJeaWafNZLnVaUgKvpLRojyE7LXt0D8KXX1q93Tc4ViXvY7D1qEyeMswHiJd9Fz7O0e6orlqcGU6JW73-rtePXBjC_-7yOigSLSbeTI_OX21PpZQZBLgD-joo0xFKwUR0uZd7hHfq9QKc4YI9U8mkLKWfv56fbRirTevkayj_n7iN-thggnHw08UBEJoEXDlX4VaI3M-gAFNx_qEtYZscuTfzYVb1gHUx6054A4EaY6MGr0uBANF5ChuhS2NrofDAIYGzLZNS4STEYGbb8SC6c-k_JIkc8gLqSI41CHI3-U54a-l1vXXjq6VcRLlTj4p8KLwhuBCTeFhj9OSpurKpoCSCDTXnat6PpuzibmzfHTlkx15yaqgF0vHws15oZxU2rL97SkS7TqUq_huj4hsLBhNBiMl5B1lsvKm7gkL_GwBjAnOKz81VRopWGL1pMhgepvzrYMiP94PCVB59ZjW1qbhaLPrVXwfajTFcQze_1TnpVBUsfhTSE_drH31rj6dUAsOJqduBWj4FVSFN0TzJVK1_nY0EZAPkFVC___njNx8iIvICEjN5Vqv4U8V8N_2sxmwQmMXZedBIl69flWUIB8rLPmox638ZZ5i0zCOe449D2H3tTEY43pUfVpMkcMBuVvyPZgh3rOa8mIlwU_eUk1p_E3JwjmhPC4bJPKmMwmQcgrssjcUHJce3lCd3oRBKpXLrr0EUAfVIwSqrxrVeG1xy821wGAm1b1xGfJBLVpN620wl0p9AQp4gew57bc3qAAvnOkrogQDaGAE0DAmdKwX_xz0e34w94_dmT_CADgLwKiTYGfxYK5evl4mHtZe0q7O_40MfhPJ_o_7noyVUMte79BxzEaEJ_CwAA?wid=1200',
}
