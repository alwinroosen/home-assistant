import { getCard } from '../utils/get-card'

export const clock = getCard({
    entity: 'device_tracker.raspberrypi',
    type: 'custom:clock-card',
})
