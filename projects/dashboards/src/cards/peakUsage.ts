import { VerticalStack } from '../utils/types'
import { getVerticalStack } from '../utils/get-vertical-stack'
import { getGauge } from '../utils/get-gauge'
import { getStatisticsGraph } from '../utils/get-statistics-graph'

const maxPeak = getGauge({
    entity: 'sensor.electricity_meter_maximum_demand_current_month',
    name: 'Piekvermogen',
    min: 0,
    max: 15,
    severity: {
        green: 0,
        yellow: 5,
        red: 7.5,
    },
})

const statistics = getStatisticsGraph({
    title: 'Piekvermogen',
    entities: [
        {
            entity: 'sensor.electricity_meter_current_average_demand',
            name: '',
        },
    ],
})

export const peakUsage: VerticalStack = getVerticalStack({
    cards: [maxPeak, statistics],
})
