import { Dashboard } from '../utils/types'
import { keuken } from '../keuken/keuken'

export const mobile: Dashboard = {
    name: 'mobile',
    title: '',
    views: [
        {
            panel: false,
            type: 'masonry',
            badges: keuken.views[0].badges,
            cards: [
                // @ts-ignore
                ...keuken.views[0].cards[0].cards[0].cards,
                // @ts-ignore
                ...keuken.views[0].cards[0].cards[1].cards,
                // @ts-ignore
                ...keuken.views[0].cards[0].cards[2].cards,
                // @ts-ignore
                ...keuken.views[0].cards[0].cards[3].cards,
                {
                    // Play stream for Karen
                    show_name: true,
                    show_icon: true,
                    type: 'button',
                    tap_action: {
                        action: 'call-service',
                        service: 'camera.play_stream',
                        target: {
                            entity_id: 'camera.uvc_g4_pro_voortuin_medium',
                        },
                        data: {
                            format: 'hls',
                            media_player: 'media_player.nesthub81df',
                        },
                    },
                    entity: 'camera.uvc_g4_pro_voortuin_medium',
                    icon: 'mdi:cctv',
                    name: 'Camera Voortuin',
                    show_state: false,
                },
                {
                    // @ts-ignore
                    show_name: true,
                    show_icon: true,
                    type: 'button',
                    tap_action: {
                        action: 'toggle',
                    },
                    entity: 'switch.shelly1_poort_kelder',
                    icon: 'mdi:garage-variant',
                    name: 'Poort Kelder',
                    show_state: false,
                    hold_action: {
                        action: 'none',
                    },
                },
            ],
        },
    ],
}
