import { Dashboard } from '../utils/types'
import { clock } from '../cards/clock'
import { kioskMode } from '../cards/kiosk-mode'
import { weatherForecast } from '../cards/weather-forecast'
import { lights } from '../cards/lights'
import { shellyPlugs } from '../cards/shelly-plugs'
import { power } from '../cards/power'
import { temperatuur } from '../cards/temperatuur'
import { voltages } from '../cards/voltages'
import { energyDistribution } from '../cards/energy-distribution'
import { automations } from '../cards/automations'
import { energy7Days } from '../cards/energy-7days'
import { cameras } from '../cards/cameras'
import { luchtontvochtigers } from '../cards/luchtontvochtigers'
import { getVerticalStack } from '../utils/get-vertical-stack'
import { getGrid } from '../utils/get-grid'
import { audi } from '../cards/audi'
import { peakUsage } from '../cards/peakUsage'
import { getHorizontalStack } from '../utils/get-horizontal-stack'
import { intex } from '../cards/intex'

const persons = getHorizontalStack({
    type: 'horizontal-stack',
    cards: [
        {
            type: 'custom:mushroom-person-card',
            entity: 'person.alwin_roosen',
            // @ts-ignore
            fill_container: false,
            layout: 'vertical',
            secondary_info: 'none',
            primary_info: 'none',
            name: 'Alwin',
            icon_type: 'entity-picture',
            view_layout: 'vertical',
        },
        {
            type: 'custom:mushroom-person-card',
            entity: 'person.karen_van_oosterwijck',
            // @ts-ignore
            fill_container: false,
            layout: 'vertical',
            secondary_info: 'none',
            primary_info: 'none',
            name: 'Karen',
            icon_type: 'entity-picture',
            view_layout: 'vertical',
        },
        {
            type: 'custom:mushroom-person-card',
            entity: 'person.wout_beersaerts',
            // @ts-ignore
            fill_container: false,
            layout: 'vertical',
            secondary_info: 'none',
            primary_info: 'none',
            name: 'Wout',
            icon_type: 'entity-picture',
            view_layout: 'vertical',
        },
        {
            type: 'custom:mushroom-person-card',
            entity: 'person.rani_aerts',
            // @ts-ignore
            fill_container: false,
            layout: 'vertical',
            secondary_info: 'none',
            primary_info: 'none',
            name: 'Rani',
            icon_type: 'entity-picture',
            view_layout: 'vertical',
        },
    ],
})

export const keuken: Dashboard = {
    name: 'keuken',
    title: '',
    views: [
        {
            panel: false,
            type: 'panel',
            // type: 'masonry',
            // layout: {
            //     width: 300,
            //     max_cols: 4,
            // },
            badges: [
                { entity: 'binary_sensor.poort_garage' },
                { entity: 'binary_sensor.poort_kelder' },
                { entity: 'binary_sensor.voordeur' },
                { entity: 'binary_sensor.achterdeur' },
                { entity: 'binary_sensor.schuifraam_living' },
            ],
            cards: [
                getGrid({
                    type: 'grid',
                    columns: 4,
                    square: false,
                    cards: [
                        getVerticalStack({
                            type: 'vertical-stack',
                            cards: [
                                clock,
                                kioskMode,
                                persons,
                                weatherForecast,
                                temperatuur,
                                // audi,
                                // intex,
                            ],
                        }),
                        getVerticalStack({
                            type: 'vertical-stack',
                            cards: [cameras, peakUsage],
                        }),
                        getVerticalStack({
                            type: 'vertical-stack',
                            cards: [energyDistribution, power, energy7Days],
                        }),
                        getVerticalStack({
                            type: 'vertical-stack',
                            cards: [
                                automations,
                                lights,
                                voltages,
                                luchtontvochtigers,
                                shellyPlugs,
                            ],
                        }),
                    ],
                }),
                // energy,
                // logbook,
                // {
                //     type: 'energy-sources-table',
                // },
                // {
                //     type: 'energy-grid-neutrality-gauge',
                // },
                // nest,
                // twc,
                // bring,
            ],
        },
    ],
}
