import { Dashboard } from './utils/types'
import { clock } from './cards/clock'
import { kioskMode } from './cards/kiosk-mode'
import { weatherForecast } from './cards/weather-forecast'
import { lights } from './cards/lights'
import { kelder } from './cards/kelder'
import { power } from './cards/power'
import { energy } from './cards/energy'
import { logbook } from './cards/logbook'
import { nest } from './cards/nest'
import { temperatuur } from './cards/temperatuur'
import { twc } from './cards/twc'
import { intex } from './cards/intex'
import { voltages } from './cards/voltages'
import { bring } from './cards/bring'
import { airco } from './cards/airco'

export const keuken: Dashboard = {
    name: 'keuken',
    title: '',
    views: [
        {
            panel: false,
            type: 'custom:masonry-layout',
            layout: {
                width: 300,
                max_cols: 4,
            },
            badges: [
                { entity: 'device_tracker.gsm_alwin' },
                { entity: 'device_tracker.gsm_karen_2' },
                { entity: 'device_tracker.gsm_wout' },
                { entity: 'binary_sensor.poort_garage' },
                { entity: 'binary_sensor.poort_kelder' },
                { entity: 'binary_sensor.voordeur' },
                { entity: 'binary_sensor.achterdeur' },
                { entity: 'binary_sensor.schuifraam_living' },
            ],
            cards: [
                clock,
                kioskMode,
                weatherForecast,
                intex,
                lights,
                kelder,
                power,
                energy,
                logbook,
                nest,
                temperatuur,
                twc,
                voltages,
                bring,
                airco,
            ],
        },
    ],
}
