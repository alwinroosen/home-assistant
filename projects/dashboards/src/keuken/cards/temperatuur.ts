import { getGlance } from '../utils/get-glance'
import { getEntity } from '../utils/get-entity'

export const temperatuur = getGlance({
    entities: [
        getEntity({
            entity: 'sensor.living_temperature',
            name: 'Living',
        }),
        getEntity({
            entity: 'sensor.keuken_temperature',
            name: 'Keuken',
        }),
        getEntity({
            entity: 'sensor.bureau_temperature',
            name: 'Bureau',
        }),
        getEntity({
            entity: 'sensor.creative_room_temperature',
            name: 'Creative Room',
        }),
        getEntity({
            entity: 'sensor.workshop_temperature',
            name: 'Werkplaats',
        }),
    ],
})
