import { getCard } from '../utils/get-card'

export const bring = getCard({
    entity: 'sensor.bring_shopping_list_winkellijstje',
    type: 'custom:bring-shopping-list-card',
    name: 'Winkellijstje',
})
