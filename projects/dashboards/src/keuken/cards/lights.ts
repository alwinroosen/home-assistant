import { getLight } from '../utils/get-light'
import { getHorizontalStack } from '../utils/get-horizontal-stack'

const keuken = getLight({
    entity: 'light.keuken',
    name: 'Keuken',
})

const eetkamer = getLight({
    entity: 'light.eetkamer',
    name: 'Eetkamer',
})

const leefruimte = getLight({
    entity: 'light.living_room',
    name: 'Living',
})

export const lights = getHorizontalStack({
    cards: [keuken, eetkamer, leefruimte],
})
