import { getCard } from '../utils/get-card'
import { getGauge } from '../utils/get-gauge'
import { getVerticalStack } from '../utils/get-vertical-stack'

const controller = getCard({
    entity: 'sensor.intex_spa_state',
    type: 'custom:intex-spa-card',
})

const power = getGauge({
    entity: 'sensor.tuya_switch_4_power',
    name: 'Jacuzzi Verbruik',
    severity: {
        green: 500,
        yellow: 30,
        red: 0,
    },
})

export const intex = getVerticalStack({
    cards: [controller, power],
})
