import { getGauge } from '../utils/get-gauge'

export const airco = getGauge({
    entity: 'sensor.tuya_switch_6_power',
    name: 'Airco Verbruik',
    severity: {
        green: 500,
        yellow: 30,
        red: 0,
    },
})
