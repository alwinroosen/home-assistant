import { getHorizontalStack } from '../utils/get-horizontal-stack'
import { getGauge } from '../utils/get-gauge'
import { Gauge } from '../utils/types'

const defaultSettings: Pick<Gauge, 'min' | 'max' | 'severity'> = {
    min: 220,
    max: 260,
    severity: {
        green: 220,
        yellow: 240,
        red: 250,
    },
}

const phase1 = getGauge({
    entity: 'sensor.voltage_phase_l1',
    name: 'L1',
    ...defaultSettings,
})

const phase2 = getGauge({
    entity: 'sensor.voltage_phase_l2',
    name: 'L2',
    ...defaultSettings,
})

const phase3 = getGauge({
    entity: 'sensor.voltage_phase_l3',
    name: 'L3',
    ...defaultSettings,
})

export const voltages = getHorizontalStack({
    cards: [phase1, phase2, phase3],
})
