import { getButton } from '../utils/get-button'
import { getGauge } from '../utils/get-gauge'
import { getEntity } from '../utils/get-entity'
import { getGrid } from '../utils/get-grid'
import { getVerticalStack } from '../utils/get-vertical-stack'
import { Gauge } from '../utils/types'

const defaultGaugeSeverity: Pick<Gauge, 'severity'> = {
    severity: {
        green: 0,
        red: 60,
        yellow: 50,
    },
}

export const kelder = getGrid({
    cards: [
        getVerticalStack({
            cards: [
                getButton({
                    entity: 'switch.80020737bcddc28b9b33',
                }),
                getGauge({
                    entity: 'sensor.creative_room_humidity',
                    name: 'Crea.Room',
                    ...defaultGaugeSeverity,
                }),
                getEntity({
                    entity: 'sensor.tuya_switch_1_power',
                    icon: ' ',
                }),
            ],
        }),
        getVerticalStack({
            cards: [
                getButton({
                    entity: 'switch.80020737b4e62d42508b',
                }),
                getGauge({
                    entity: 'sensor.wapenkamer_humidity',
                    name: 'Wapenkamer',
                    ...defaultGaugeSeverity,
                }),
                getEntity({
                    entity: 'sensor.tuya_switch_7_power',
                    icon: ' ',
                }),
            ],
        }),
        getVerticalStack({
            cards: [
                getButton({
                    entity: 'switch.37685021bcddc28b9c68',
                }),
                getGauge({
                    entity: 'sensor.magazijn_humidity',
                    name: 'Magazijn',
                    ...defaultGaugeSeverity,
                }),
                getEntity({
                    entity: 'sensor.tuya_switch_3_power',
                    icon: ' ',
                }),
            ],
        }),
        getVerticalStack({
            cards: [
                getButton({
                    entity: 'switch.40605414bcddc28b9bb5',
                }),
                getGauge({
                    entity: 'sensor.workshop_humidity',
                    name: 'Werkplaats',
                    ...defaultGaugeSeverity,
                }),
                getEntity({
                    entity: 'sensor.tuya_switch_5_power',
                    icon: ' ',
                }),
            ],
        }),
    ],
})
