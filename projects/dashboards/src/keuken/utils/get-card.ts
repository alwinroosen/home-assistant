import { Card } from './types'

type GetCard = Pick<Card, 'entity'> & Partial<Card>

export const getCard = (config: GetCard): Card => {
    return {
        type: 'card',
        ...config,
    }
}
