import { Light } from './types'

type GetLight = Pick<Light, 'entity' | 'name'> & Partial<Light>

export const getLight = (config: GetLight): Light => {
    return {
        type: 'light',
        hold_action: {
            action: 'more-info',
        },
        ...config,
    }
}
