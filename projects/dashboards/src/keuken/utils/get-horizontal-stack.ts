import { HorizontalStack } from './types'

type GetHorizontalStack = Pick<HorizontalStack, 'cards'> &
    Partial<HorizontalStack>

export const getHorizontalStack = (
    config: GetHorizontalStack,
): HorizontalStack => {
    return {
        type: 'horizontal-stack',
        ...config,
    }
}
