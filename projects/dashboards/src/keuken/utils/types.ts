export interface Glance extends Card {
    type: 'glance'
    entities: Entity[]
    show_icon: boolean
}

export interface Thermostat extends Card {
    type: 'thermostat'
}

export interface Logbook extends Card {
    type: 'logbook'
    entities: string[]
    hours_to_show: number
    title: string
}

export interface Entities extends Card {
    type: 'entities'
    entities: {
        entity: string
        name: string
    }[]
    title: string
}

export interface HorizontalStack extends Card {
    type: 'horizontal-stack'
    cards: Cards
}

export interface VerticalStack extends Card {
    type: 'vertical-stack'
    cards: Cards
}

export interface Grid extends Card {
    type: 'grid'
    cards: Cards
    columns: number
    square: boolean
}

export interface Button extends Card {
    type: 'button'
    entity: string
    show_name: boolean
    show_state: boolean
    icon_height: string
}

export interface Gauge extends Card {
    type: 'gauge'
    entity: string
    max: number
    min: number
    name: string
    severity?: {
        green: number
        red: number
        yellow: number
    }
}

export interface Light extends Card {
    type: 'light'
    entity: string
    hold_action?: {
        action: string
    }
}

export interface Entity extends Card {
    type: 'entity'
    icon?: string
}

export interface Card {
    entity?: string
    type: string
    name?: string
}

export type Cards = Card[]

export interface Badge {
    entity: string
}

export type Badges = Badge[]

export interface Layout {
    width: number
    max_cols: number
}

export interface View {
    panel: boolean
    type: 'custom:masonry-layout'
    layout: Layout
    badges: Badges
    cards: Cards
}

export interface Dashboard {
    name: string
    title: string
    views: View[]
}

export type Dashboards = Dashboard[]
