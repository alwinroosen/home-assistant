import { Entity } from './types'

type GetEntity = Pick<Entity, 'entity'> & Partial<Entity>

export const getEntity = (config: GetEntity): Entity => {
    return {
        type: 'entity',
        name: 'Verbruik',
        ...config,
    }
}
