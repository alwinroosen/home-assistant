import { Dashboard } from '../utils/types'
import { shellyPlugs } from '../cards/shelly-plugs'
import { power } from '../cards/power'
import { energy } from '../cards/energy'
import { twc } from '../cards/twc'
import { voltages } from '../cards/voltages'

export const energie: Dashboard = {
    name: 'energie',
    title: '',
    views: [
        {
            panel: false,
            type: 'custom:masonry-layout',
            layout: {
                width: 300,
                max_cols: 4,
            },
            badges: [],
            cards: [shellyPlugs, power, energy, twc, voltages],
        },
    ],
}
